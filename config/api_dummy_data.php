<?php
return [
    "user_register" => [
        "email"       => str_random(5) . "@test.com",
        "password"    => "password",
        "vendor_id"   => "1",
        "first_name"  => str_random(3),
        "last_name"   => str_random(3),
        "profile_pic" => public_path() . "/profile-pictures.png",
    ],
    "create_event"  => [
        "code"           => [],
        "name"           => "Dan's event",
        "vendor_id"      => 1,
        "user_id"        => 0,
        "start_date"     => "2016-08-15 11:00:00",
        "end_date"       => "2016-08-15 19:00:00",
        "details"        => "Event description",
        "is_live"        => false,
        "livestream_url" => "",
        "lat"            => 12.12441,
        "lon"            => 56.3223562,
        "location_name"  => "Homestead",
        "address"        => "31 Main Street",
        "timezone"       => "Europe/Bucharest",
    ],
    "create_style" => [
        "bg_color"=>"#ffffff",
        "font_family"=>"Times New Roman",
        "font_color"=>"#eee",
        'style_type' => "event_style",
        'delete_file' => 1,
        "file" => public_path() . "/profile-pictures.png",
    ],
    "upload_video" => [
        "file" => public_path() . "/small.mp4"
    ],
    "upload_videos" => [
            public_path() . "/small.mp4",
            public_path() . "/small.mp4",
            public_path() . "/small.mp4"
    ],
    "upload_file" => [
        "file" => public_path() . "/profile-pictures.png"
    ],
    "participants" => [
        "total_participants" => 3
    ],
    "rsvps" => [
        "total_participants" => 3,
        "status" => 2
    ],
    "chat_id" => [
        "dialog_id" => "new_dialog_id"
    ]
];
