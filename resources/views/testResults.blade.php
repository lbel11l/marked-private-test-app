<html>
<head>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script src="https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js"></script>
</head>
<body>
<div class="container-fluid">
    @foreach($testResults as $testName => $testResultFunctions)
        <h1>{{$testName}}</h1>
            @foreach($testResultFunctions as $functionName=>$results)
                <div class="row">
                    <button type="button" class="btn @if(!isset($results['Error'])) btn-success @else btn-danger @endif"
                            data-toggle="collapse"
                            data-target="#{{$functionName}}">{{$functionName}}</button>

                    @if(isset($results['Error']))<span>Failed</span>@endif
                        <div id="{{$functionName}}" class="collapse">
                            <div class="row">
                                <div class="col-md-4">
                                    @if(isset($results['Input']))
                                        <p>Input</p>
                                        <pre class="prettyprint bg-success">
                                        {{print_r($results['Input'])}}
                                    </pre>
                                    @endif
                                </div>
                                <div class="col-md-8">
                                    @if(isset($results['Output']))
                                        <p>Output</p>
                                        <pre class="prettyprint bg-success">
                                        {{print_r($results['Output'])}}
                                    </pre>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    @if(isset($results['Error']))
                                        <p>ErrorData</p>
                                        <pre class="prettyprint bg-danger">
                                        {{print_r($results['Error'])}}
                                </pre>
                                    @endif
                                </div>
                            </div>
                        </div>
                </div>
            @endforeach
    @endforeach
</div>
</body>
</html>
