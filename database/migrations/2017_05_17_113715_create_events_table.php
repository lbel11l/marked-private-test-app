<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('isHost');
            $table->integer('vendor_id');
            $table->integer('vendor_event_id')->nullable();
            $table->string('name');
            $table->integer('user_id');
            $table->string('start_date');
            $table->string('end_date');
            $table->string('details');
            $table->boolean('is_live');
            $table->string('livestream_url')->nullable();
            $table->string('lat');
            $table->string('lon');
            $table->string('location_name');
            $table->string('dialog_id')->nullable();
            $table->string('gallery')->nullable();
            $table->string('smashboard')->nullable();
            $table->string('descriptionasset')->nullable();
            $table->text('hostcodes')->nullable();
            $table->string('guestcodes')->nullable();
            $table->text('rsvps')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
