<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vendor_user_id')->nullable();
            $table->integer('vendor_id')->nullable();
            $table->string('email')->unique();
            $table->integer('last_event_id')->nullable();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('birthday')->nullable();
            $table->string('gender')->nullable();
            $table->string('api_token')->nullable();
            $table->text('profile_pic')->nullable();
            $table->text('chat_credentials')->nullable();
            $table->string('type')->nullable();
            $table->string('password')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
