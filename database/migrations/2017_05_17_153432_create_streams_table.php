<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStreamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('streams', function (Blueprint $table) {
            $table->string('stream_name')->nullable();
            $table->string('publish_token')->nullable();
            $table->string('read_token')->nullable();
            $table->text('server')->nullable();
            $table->text('stream_urls')->nullable();
            $table->string('vendor_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->string('event_id')->nullable();

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('streams');
    }
}
