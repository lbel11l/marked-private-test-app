<?php
/**
 * Created by PhpStorm.
 * User: sergiu
 * Date: 5/5/17
 * Time: 11:30 AM
 */

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $fillable = [
        "id",
        "file_id",
        "event_id",
        "file",

    ];

    protected $casts = [
        "file" => "array",
    ];
}
