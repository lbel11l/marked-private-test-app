<?php
/**
 * Created by PhpStorm.
 * User: sergiu
 * Date: 5/5/17
 * Time: 11:30 AM
 */

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class Rsvp extends Model
{
    protected $fillable = [
        "id",
        "user_id",
        "vendor_id",
        "event_id",
        "status",
        "total_participants",
    ];
}
