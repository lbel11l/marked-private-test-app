<?php
/**
 * Created by PhpStorm.
 * User: sergiu
 * Date: 5/5/17
 * Time: 11:30 AM
 */

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class Stream extends Model
{
    protected $fillable = [
        "stream_name",
        "publish_token",
        "read_token",
        "server",
        "stream_urls",
        "vendor_id",
        "user_id",
        "event_id"

    ];

    protected $casts = [
        "server" => "array",
        "stream_urls" => "array"
    ];
}
