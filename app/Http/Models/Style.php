<?php
/**
 * Created by PhpStorm.
 * User: sergiu
 * Date: 5/5/17
 * Time: 11:30 AM
 */

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class Style extends Model
{
    protected $fillable = [
        "id",
        "user_id",
        "vendor_id",
        "file",
        "bg_color",
        "font_family",
        "font_color"

    ];

    protected $casts = [
        "file" => "array",
    ];
}
