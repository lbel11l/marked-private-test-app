<?php
/**
 * Created by PhpStorm.
 * User: sergiu
 * Date: 5/5/17
 * Time: 11:30 AM
 */

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = [
        "id",
        "isHost",
        "vendor_id",
        "vendor_event_id",
        "name",
        "user_id",
        "start_date",
        "end_date",
        "details",
        "is_live",
        "livestream_url",
        "created_at",
        "updated_at",
        "lat",
        "lon",
        "location_name",
        "dialog_id",
        "gallery",
        "smashboard",
        "descriptionasset",
        "hostcodes",
        "guestcodes",
        "rsvps"
    ];

    protected $casts = [
        "hostcodes" => "array",
        "rsvps" => "array"
    ];

}
