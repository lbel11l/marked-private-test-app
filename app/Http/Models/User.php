<?php
/**
 * Created by PhpStorm.
 * User: sergiu
 * Date: 5/5/17
 * Time: 11:30 AM
 */

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class User extends \App\User
{

    protected $fillable = [
        'id',
        'vendor_user_id',
        'vendor_id',
        'email',
        'last_event_id',
        'first_name',
        'last_name',
        'birthday',
        'gender',
        'api_token',
        'profile_pic',
        'chat_credentials',
        'type'
    ];
    protected $casts = [
        'profile_pic' => 'array',
        'chat_credentials'=> 'array'
    ];
}
