<?php
/**
 * Created by PhpStorm.
 * User: sergiu
 * Date: 5/5/17
 * Time: 11:30 AM
 */

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $fillable = [
        "vendor_id",
        "message",
        "full_name",
        "email",
        "id",
        "sent",

    ];
}
