<?php
/**
 * Created by PhpStorm.
 * User: sergiu
 * Date: 4/25/17
 * Time: 2:09 PM
 */

namespace App\Http\Controllers;

use App\Services\Tests\HostCodeTest;
use App\Services\Tests\UserLoginTest;
use App\Services\Tests\UserRegisterTest;
use App\Services\Tests\UserTest;
use App\Services\Workflows\CodesWorkflow;
use App\Services\Workflows\EventFlow;
use App\Services\Workflows\GalleryFlow;
use App\Services\Workflows\HostFlow;
use App\Services\Workflows\LogoutFlow;
use App\Services\Workflows\RegisterLoginFlow;
use App\Services\Workflows\RsvpFlow;
use App\Services\Workflows\StreamFlow;
use App\Services\Workflows\StyleFlow;
use App\Services\Workflows\TicketFlow;
use View;

class TestingController extends Controller
{


    protected $tests = [
//        UserTest::class,
//        HostCodeTest::class
//        "CodeWorkFlow" => CodesWorkflow::class,
//        "HostFlow"=>HostFlow::class,
        "RegisterLoginFlow" => RegisterLoginFlow::class,
        "EventFlow" => EventFlow::class,
        "StreamFlow" => StreamFlow::class,
        "TicketFlow" => TicketFlow::class,
        "StyleFlow" => StyleFlow::class,
        "GalleryFlow" => GalleryFlow::class,
        "RsvpFlow" => RsvpFlow::class,
        "LogoutFlow" => LogoutFlow::class,
    ];

    public function index()
    {
//        $userRequestsService = app()->make(UserRegisterTest::class);

//        dd($userRequestsService->getResult());
    }

    public function runTests()
    {
        $testResults = [];

        foreach ($this->tests as $testName=>$test) {
            $workflowTest = app()->make($test);
            $workFlowData = $workflowTest->execute();
            $testResults[$testName] = $workFlowData;
        }
        return View::make('testResults')->with(["testResults" => $testResults]);
    }

}
