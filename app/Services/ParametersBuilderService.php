<?php

namespace App\Services;

use GuzzleHttp\Psr7\UploadedFile;
use File;

class ParametersBuilderService
{

    public function paramsToMultipart($params)
    {
        $multipart = [];

        foreach ($params as $name => $value) {
            $multipart[] = [
                'name' => $name,
                'contents' => $value instanceof UploadedFile ? $value->getStream() : $value,
            ];
        }

        return $multipart;
    }

    public function pathToUploadedFile($path)
    {
        return new UploadedFile($path, File::size($path), 0, File::name($path), File::mimeType($path));
    }
}
