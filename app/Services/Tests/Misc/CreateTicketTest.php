<?php

namespace App\Services\Tests;

use App\Services\Api\TicketRequestsService;
use App\Services\Api\UserRequestsService;

class CreateTicketTest extends WorkflowTestAbstract
{
    const TEST_NAME = "Create Ticket";
    protected $userRequestsService;
    protected $ticketRequestsService;
    protected $userData;

    protected $steps = [
        "Create Ticket" => [],
    ];

    public function __construct(
        $userData,
        UserRequestsService $userRequestsService,
        TicketRequestsService $ticketRequestsService
    )
    {
        $this->userData = $userData;
        $this->setTestName(self::TEST_NAME);
        $this->userRequestsService = $userRequestsService;
        $this->ticketRequestsService = $ticketRequestsService;
        $this->runTest();
    }

    public function testCreateTicket()
    {
        $inputData = [
            'vendor_id' => $this->userData->vendor_id,
            'message' => "This is an example message",
            'full_name' => $this->userData->first_name . " " . $this->userData->last_name,
            'email' => $this->userData->email
        ];
        $this->setStepInput($inputData);
        $this->ticketRequestsService->setApiKey($this->userData->api_token);
        $response = $this->ticketRequestsService->createTicket($inputData);
        $this->setStepOutput(json_decode($response->getContent(), true));
        $response->assertJson(
            [
                'id' => true,
                'vendor_id' => $inputData['vendor_id'],
                'email' => $inputData['email'],
                'message' => $inputData['message'],
                'full_name' => $inputData['full_name'],
            ]
        );
    }
}
