<?php

namespace App\Services\Tests;

use App\Services\Api\HostCodesRequestsService;
use App\Services\Api\StreamRequestsService;
use App\Services\Api\StyleRequestService;
use App\Services\Api\TicketRequestsService;
use App\Services\Api\UserRequestsService;
use App\Services\ParametersBuilderService;

class CreateDescriptionAssetTest extends WorkflowTestAbstract
{
    const TEST_NAME = "Create Description Asset";
    protected $userData;
    protected $eventData;
    protected $userRequestsService;
    protected $parametersBuilderService;

    protected $steps = [
        "Create Description Asset" => [],
    ];

    public function __construct(
        $userData,
        $eventData,
        UserRequestsService $userRequestsService,
        ParametersBuilderService $parametersBuilderService
    )
    {
        $this->userData = $userData;
        $this->eventData = $eventData;
        $this->setTestName(self::TEST_NAME);
        $this->userRequestsService = $userRequestsService;
        $this->parametersBuilderService = $parametersBuilderService;
        $this->runTest();
    }

    public function testCreateDescriptionAsset()
    {

        $inputData = array_merge(
            config("api_dummy_data.upload_file"),
            [
                'vendor_id' => $this->userData->vendor_id
            ]
        );
        $this->setStepInput($inputData);
        $inputData['file'] = $this->parametersBuilderService->pathToUploadedFile($inputData['file']);
        $this->userRequestsService->setApiKey($this->userData->api_token);
        $response = $this->userRequestsService->createDescriptionAsset($this->eventData->id, $inputData);
        $this->setStepOutput(json_decode($response->getContent(), true));
//        $response->assertJson(
//            [
//                'id'               => true,
//                'vendor_id'        => $inputData['vendor_id']
//            ]
//        );
    }
}
