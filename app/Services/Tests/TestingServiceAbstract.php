<?php

namespace App\Services\Tests;


class TestingServiceAbstract
{
    protected $testResults;

    protected $tests = [
        //CODES
        "hostCode" => HostCodeTest::class,
        "addGuestCode" => AddGuestCodeTest::class,
        //USER
        "userRegister" => UserRegisterTest::class,
        "userLogin" => UserLoginTest::class,
        "userEdit" => UserEditTest::class,
        "loadMultipleUsers" => LoadMultipleUsersTest::class,
        "userLogout" => UserLogoutTest::class,
        "changePassword" => UserChangePasswordTest::class,
        //EVENTS
        "userEvents" => UserEventsTest::class,
        "getAllUserEvents" => GetAllUserEventsTest::class,
        "createEvent" => EventCreateTest::class,
        "getEvent" => GetEventTest::class,
        "updateEvent" => EventUpdateTest::class,
        "getEventsBackgrounds" => GetEventsBackgroundsTest::class,
        "loadMultipleEventObjects" => LoadMultipleEventObjectsTest::class,
        //DESCRIPTION ASSETS
        "createDescriptionAsset" => CreateDescriptionAssetTest::class,
        "getDescriptionAsset" => GetDescriptionAssetTest::class,
        //STREAM
        "createStream" => CreateStreamTest::class,
        "getStream" => GetStreamTest::class,
        "deleteStream" => DeleteStreamTest::class,
        //TICKET
        "createTicket" => CreateTicketTest::class,
        //STYLE
        "createStyle" => CreateStyleTest::class,
        "createEventStyle" => CreateEventStyleTest::class,
        "getStyle" => GetStyleTest::class,
        "editStyle" => EditStyleTest::class,
        "deleteStyle" => DeleteStyleTest::class,
        //GALLERY
        "createImageGallery" => CreateImageGalleryTest::class,
        "createVideoGallery" => CreateVideoGalleryTest::class,
        "getAllEventGalleries" => GetAllEventGalleriesTest::class,
        "updateGallery" => UpdateGalleryTest::class,
        "updateImageGallery" => UpdateImageGalleryTest::class,
        "deleteGallery" => DeleteGalleryTest::class,
        "galleryVideosCreateMultipleCopy" => GalleryVideosCreateMultipleCopyTest::class,
        "deleteMultipleGalleries" => DeleteMultipleGalleriesTest::class,
        //RSVP
        "getAllEventRSVP" => GetAllEventRSVPTest::class,
        "getEventRSVP" => GetEventRSVPTest::class,
        "updateEventRSVP" => UpdateEventRSVPTest::class,
        //CHAT
        "rebuildChatDialog" => RebuildChatDialogTest::class,
        "changeChatDialogId" => ChangeChatDialogIdTest::class,
    ];

    public function runTest($testName, $arguments = [])
    {
        $workflowTest = app($this->tests[$testName], $arguments);
        $result = $workflowTest->getResult();
        $this->testResults[$testName] = $result;

        return $this->testResults;
    }

}
