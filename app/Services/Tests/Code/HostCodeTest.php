<?php

namespace App\Services\Tests;

use App\Services\Api\HostCodesRequestsService;
use App\Services\Api\UserRequestsService;
use App\Services\ParametersBuilderService;

class HostCodeTest extends WorkflowTestAbstract
{
    const TEST_NAME = "Host Code Test";

    protected $hostCodesRequestsService;

    protected $steps = [
        "Host Code Validation" => [],
    ];

    public function __construct(
        HostCodesRequestsService $hostCodesRequestsService
    ) {
        $this->hostCodesRequestsService = $hostCodesRequestsService;
        $this->setTestName(self::TEST_NAME);
        $this->runTest();
    }

    public function testHostCodeValidation()
    {
        $code = $this->hostCodesRequestsService->getValidHostCode()->code;
        $this->setStepInput(["code " => $code]);
        $response = $this->hostCodesRequestsService->checkCodes([$code]);
        $this->setStepOutput(json_decode($response->getContent(), true));
        $this->setVariables("hostCode", $code);
        $response->assertJson(
            [
                $code => [
                    'code'    => $code,
                    'status'  => 0,
                    'user_id' => 0,
                ]
            ]
        );
    }

}
