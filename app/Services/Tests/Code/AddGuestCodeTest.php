<?php

namespace App\Services\Tests;

use App\Services\Api\EventRequestsService;
use App\Services\Api\HostCodesRequestsService;

class AddGuestCodeTest extends WorkflowTestAbstract
{
    const TEST_NAME = "Add Guest Code";
    protected $eventRequestService;
    protected $userData;
    protected $eventData;
    protected $hostCodesRequestsService;
    protected $codeData;

    protected $steps = [
        "Add Guest Code"        => [],
    ];

    public function __construct(
        $userData,
        $eventData,
        HostCodesRequestsService $hostCodesRequestsService,
        EventRequestsService $eventRequestService
    ) {
        $this->hostCodesRequestsService = $hostCodesRequestsService;
        $this->eventRequestService = $eventRequestService;
        $this->userData = $userData;
        $this->eventData = $eventData;
        $this->setTestName(self::TEST_NAME);
        $this->runTest();
    }

    public function testAddGuestCode()
    {
        $guestCode = $this->eventData->hostcodes['guestcodes'][0]['code'];
        $inputData = array_merge(
            config("api_dummy_data.participants"),
            ["code" => $guestCode]
        );
        $this->setStepInput($inputData);
        $this->eventRequestService->setApiKey($this->userData->api_token);
        $response = $this->eventRequestService->addGuestCode($this->eventData->id, $guestCode);
        $this->setStepOutput(json_decode($response->getContent(), true));

    }

}
