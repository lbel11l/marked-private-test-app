<?php

namespace App\Services\Tests;

use App\Services\Api\EventRequestsService;

class UpdateEventRSVPTest extends WorkflowTestAbstract
{
    const TEST_NAME = "Update Event RSVP";
    protected $eventRequestsService;
    protected $userData;
    protected $eventData;

    protected $steps = [
        "Update Event RSVP" => [],
    ];

    public function __construct(
        $userData,
        $eventData,
        EventRequestsService $eventRequestsService
    )
    {
        $this->eventRequestsService = $eventRequestsService;
        $this->userData = $userData;
        $this->eventData = $eventData;
        $this->setTestName(self::TEST_NAME);
        $this->runTest();
    }

    public function testUpdateEventRSVP()
    {
        $inputData = array_merge(
            config("api_dummy_data.rsvps"),
            ['eventId' => $this->eventData->id]
        );
        $this->eventRequestsService->setApiKey($this->userData->api_token);
        $this->setStepInput($inputData);
        $response = $this->eventRequestsService->updateEventRSVP($this->eventData->id, $this->eventData->rsvps[0]['id'], $inputData);
        $this->setStepOutput(json_decode($response->getContent(), true));
    }
}
