<?php

namespace App\Services\Tests;

use App\Services\Api\EventRequestsService;

class GetAllEventRSVPTest extends WorkflowTestAbstract
{
    const TEST_NAME = "Get All Event RSVP";
    protected $eventRequestsService;
    protected $userData;
    protected $eventData;

    protected $steps = [
        "Get All Event RSVP" => [],
    ];

    public function __construct(
        $userData,
        $eventData,
        EventRequestsService $eventRequestsService
    )
    {
        $this->eventRequestsService = $eventRequestsService;
        $this->userData = $userData;
        $this->eventData = $eventData;
        $this->setTestName(self::TEST_NAME);
        $this->runTest();
    }

    public function testGetAllEventRSVP()
    {
        $this->eventRequestsService->setApiKey($this->userData->api_token);
        $this->setStepInput(['eventId' => $this->eventData->id]);
        $response = $this->eventRequestsService->getAllEventRSVP($this->eventData->id);
        $this->setStepOutput(json_decode($response->getContent(), true));
    }
}
