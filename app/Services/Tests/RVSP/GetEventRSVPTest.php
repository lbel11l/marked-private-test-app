<?php

namespace App\Services\Tests;

use App\Services\Api\EventRequestsService;

class GetEventRSVPTest extends WorkflowTestAbstract
{
    const TEST_NAME = "Get Event RSVP";
    protected $eventRequestsService;
    protected $userData;
    protected $eventData;

    protected $steps = [
        "Get Event RSVP" => [],
    ];

    public function __construct(
        $userData,
        $eventData,
        EventRequestsService $eventRequestsService
    )
    {
        $this->eventRequestsService = $eventRequestsService;
        $this->userData = $userData;
        $this->eventData = $eventData;
        $this->setTestName(self::TEST_NAME);
        $this->runTest();
    }

    public function testGetEventRSVP()
    {
        $this->eventRequestsService->setApiKey($this->userData->api_token);
        $this->setStepInput(['eventId' => $this->eventData->id]);
        $response = $this->eventRequestsService->getEventRSVP($this->eventData->id, $this->eventData->rsvps[0]['id']);
        $this->setStepOutput(json_decode($response->getContent(), true));
    }
}
