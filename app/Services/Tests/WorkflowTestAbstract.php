<?php

namespace App\Services\Tests;

use Illuminate\Support\Facades\App;

abstract class WorkflowTestAbstract
{
    protected $testName;
    protected $steps = [];
    protected $result;

    public function runTest()
    {
        $currentStep = 1;
        $this->result['Title'] = $this->getTestName();
        do {
            $currentStepName = array_keys($this->steps)[$currentStep - 1];
            try {
                App::call([$this, "test" . str_replace(" ", "", $currentStepName)]);
            } catch (\Exception $exception) {
                $this->setStepError($exception->getMessage());
            }
            $currentStep++;
        } while ($currentStep <= count($this->steps));
    }

    /**
     * @param mixed $testName
     */
    public function setTestName($testName)
    {
        $this->testName = $testName;
    }

    /**
     * @return mixed
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @return mixed
     */
    public function getTestName()
    {
        return $this->testName;
    }

    public function setStepInput($inputData)
    {
        $this->result['Input'] = $inputData;
    }

    public function setStepOutput($outputData)
    {
        $this->result['Output'] = $outputData;
    }

    public function setStepError($errorData)
    {
        $this->result['Error'] = $errorData;
    }

    public function setVariables($variableName, $variableValue)
    {
        $this->result['Variables'][$variableName] = $variableValue;
    }

}
