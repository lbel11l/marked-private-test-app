<?php

namespace App\Services\Tests;

use App\Services\Api\StreamRequestsService;
use App\Services\ParametersBuilderService;

class GalleryVideosCreateMultipleCopyTest extends WorkflowTestAbstract
{
    const TEST_NAME = "Gallery Videos Create Multiple Copy";
    protected $userData;
    protected $eventData;
    protected $streamRequestService;
    protected $parametersBuilderService;

    protected $steps = [
        "Gallery Videos Create Multiple Copy"        => [],
    ];

    public function __construct(
        $userData,
        $eventData,
        StreamRequestsService $streamRequestService,
        ParametersBuilderService $parametersBuilderService
    ) {
        $this->userData = $userData;
        $this->eventData = $eventData;
        $this->setTestName(self::TEST_NAME);
        $this->streamRequestService = $streamRequestService;
        $this->parametersBuilderService = $parametersBuilderService;
        $this->runTest();
    }

    public function testGalleryVideosCreateMultipleCopy()
    {
        $files = config("api_dummy_data.upload_videos");
        foreach ($files as $index=>$file){
            $inputData['files['.$index.']'] = $this->parametersBuilderService->pathToUploadedFile($file);
        }
        $inputData['vendor_id'] = $this->userData->vendor_id;
        $inputData['user_id'] = $this->userData->id;
        $this->setStepInput($inputData);
        $this->streamRequestService->setApiKey($this->userData->api_token);
        $response = $this->streamRequestService->galleryVideosCreateMultipleCopy($this->eventData->id, $inputData);
        $this->setStepOutput(json_decode($response->getContent(), true));
//        $response->assertJson(
//            [
//                'id'               => true,
//                'vendor_id'        => $inputData['vendor_id']
//            ]
//        );
    }
}
