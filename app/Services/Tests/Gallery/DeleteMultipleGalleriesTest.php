<?php

namespace App\Services\Tests;

use App\Services\Api\EventRequestsService;
use App\Services\Api\UserRequestsService;

class DeleteMultipleGalleriesTest extends WorkflowTestAbstract
{
    const TEST_NAME = "Delete Multiple Galleries";
    protected $userRequestsService;
    protected $eventRequestsService;
    protected $userData;
    protected $eventData;
    protected $galleryData;

    protected $steps = [
        "Delete Multiple Galleries" => [],
    ];

    public function __construct(
        $userData,
        $eventData,
        $galleryData,
        UserRequestsService $userRequestsService,
        EventRequestsService $eventRequestsService
    )
    {
        $this->userData = $userData;
        $this->eventData = $eventData;
        $this->galleryData = $galleryData;
        $this->setTestName(self::TEST_NAME);
        $this->userRequestsService = $userRequestsService;
        $this->eventRequestsService = $eventRequestsService;
        $this->runTest();
    }

    public function testDeleteMultipleGalleries()
    {
        $galleryIds = [
            $this->galleryData->id,
            $this->galleryData->id - 1,
        ];
        $this->eventRequestsService->setApiKey($this->userData->api_token);
        $this->setStepInput(["ids" => $galleryIds]);
        $response = $this->eventRequestsService->deleteMultipleGalleries($this->eventData->id, $galleryIds);
        $this->setStepOutput(json_decode($response->getContent(), true));
        $response->assertJson(
            [
                'message' => "Entity deleted"
            ]
        );
    }
}
