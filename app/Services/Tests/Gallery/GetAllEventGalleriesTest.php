<?php

namespace App\Services\Tests;

use App\Services\Api\EventRequestsService;

class GetAllEventGalleriesTest extends WorkflowTestAbstract
{
    const TEST_NAME = "Get All Event Galleries";
    protected $eventRequestsService;
    protected $userData;
    protected $eventData;

    protected $steps = [
        "Get All Event Galleries" => [],
    ];

    public function __construct(
        $userData,
        $eventData,
        EventRequestsService $eventRequestsService
    )
    {
        $this->eventRequestsService = $eventRequestsService;
        $this->userData = $userData;
        $this->eventData = $eventData;
        $this->setTestName(self::TEST_NAME);
        $this->runTest();
    }

    public function testGetAllEventGalleries()
    {
        $this->eventRequestsService->setApiKey($this->userData->api_token);
        $this->setStepInput(['eventId' => $this->eventData->id]);
        $response = $this->eventRequestsService->getAllEventGalleries($this->eventData->id);
        $this->setStepOutput(json_decode($response->getContent(), true));
    }
}
