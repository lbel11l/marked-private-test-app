<?php

namespace App\Services\Tests;

use App\Services\Api\EventRequestsService;
use App\Services\Api\UserRequestsService;

class DeleteGalleryTest extends WorkflowTestAbstract
{
    const TEST_NAME = "Delete Gallery";
    protected $userRequestsService;
    protected $eventRequestsService;
    protected $userData;
    protected $eventData;
    protected $galleryData;

    protected $steps = [
        "Delete Gallery" => [],
    ];

    public function __construct(
        $userData,
        $eventData,
        $galleryData,
        UserRequestsService $userRequestsService,
        EventRequestsService $eventRequestsService
    )
    {
        $this->userData = $userData;
        $this->eventData = $eventData;
        $this->galleryData = $galleryData;
        $this->setTestName(self::TEST_NAME);
        $this->userRequestsService = $userRequestsService;
        $this->eventRequestsService = $eventRequestsService;
        $this->runTest();
    }

    public function testDeleteGallery()
    {
        $this->setStepInput([
            "event_id" => $this->eventData->id,
            "gallery_id" => $this->galleryData->id
            ]
        );
        $this->eventRequestsService->setApiKey($this->userData->api_token);
        $response = $this->eventRequestsService->deleteGallery($this->eventData->id, $this->galleryData->id);
        $this->setStepOutput(json_decode($response->getContent(), true));
        $response->assertJson(
            [
                'message' => "Entity deleted"
            ]
        );
    }
}
