<?php

namespace App\Services\Tests;

use App\Services\Api\StreamRequestsService;
use App\Services\ParametersBuilderService;

class CreateVideoGalleryTest extends WorkflowTestAbstract
{
    const TEST_NAME = "Create Video Gallery";
    protected $userData;
    protected $eventData;
    protected $streamRequestService;
    protected $parametersBuilderService;

    protected $steps = [
        "Create Video Gallery" => [],
    ];

    public function __construct(
        $userData,
        $eventData,
        StreamRequestsService $streamRequestService,
        ParametersBuilderService $parametersBuilderService
    )
    {
        $this->userData = $userData;
        $this->eventData = $eventData;
        $this->setTestName(self::TEST_NAME);
        $this->streamRequestService = $streamRequestService;
        $this->parametersBuilderService = $parametersBuilderService;
        $this->runTest();
    }

    public function testCreateVideoGallery()
    {

        $inputData = array_merge(
            config("api_dummy_data.upload_video"),
            [
                'vendor_id' => $this->userData->vendor_id
            ]
        );
        $this->setStepInput($inputData);
        $inputData['file'] = $this->parametersBuilderService->pathToUploadedFile($inputData['file']);
        $this->streamRequestService->setApiKey($this->userData->api_token);
        $response = $this->streamRequestService->createVideoGallery($this->eventData->id, $inputData);
        $this->setStepOutput(json_decode($response->getContent(), true));
        $response->assertJson(
            [
                'id'       => true,
                'file_id'  => true,
                'event_id' => true,
            ]
        );
    }
}
