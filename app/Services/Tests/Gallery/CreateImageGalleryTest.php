<?php

namespace App\Services\Tests;

use App\Services\Api\ImageRequestsService;
use App\Services\ParametersBuilderService;

class CreateImageGalleryTest extends WorkflowTestAbstract
{
    const TEST_NAME = "Create Image Gallery";
    protected $userData;
    protected $eventData;
    protected $imageRequestService;
    protected $parametersBuilderService;

    protected $steps = [
        "Create Image Gallery" => [],
    ];

    public function __construct(
        $userData,
        $eventData,
        ImageRequestsService $imageRequestService,
        ParametersBuilderService $parametersBuilderService
    )
    {
        $this->userData = $userData;
        $this->eventData = $eventData;
        $this->setTestName(self::TEST_NAME);
        $this->imageRequestService = $imageRequestService;
        $this->parametersBuilderService = $parametersBuilderService;
        $this->runTest();
    }

    public function testCreateImageGallery()
    {

        $inputData = array_merge(
            config("api_dummy_data.upload_file"),
            [
                'vendor_id' => $this->userData->vendor_id
            ]
        );
        $this->setStepInput($inputData);
        $inputData['file'] = $this->parametersBuilderService->pathToUploadedFile($inputData['file']);
        $this->imageRequestService->setApiKey($this->userData->api_token);
        $response = $this->imageRequestService->createImageGallery($this->eventData->id, $inputData);
        $this->setStepOutput(json_decode($response->getContent(), true));
        $response->assertJson(
            [
                'id'       => true,
                'file_id'  => true,
                'event_id' => true,
            ]
        );
    }
}
