<?php

namespace App\Services\Tests;

use App\Services\Api\EventRequestsService;
use App\Services\Api\UserRequestsService;

class LoadMultipleEventObjectsTest extends WorkflowTestAbstract
{
    const TEST_NAME = "Load Multiple Event Objects";
    protected $eventRequestsService;
    protected $userRequestsService;
    protected $userData;
    protected $eventData;

    protected $steps = [
        "Load Multiple Event Objects" => [],
    ];

    public function __construct(
        $userData,
        $eventData,
        UserRequestsService $userRequestsService,
        EventRequestsService $eventRequestsService
    )
    {
        $this->userData = $userData;
        $this->eventData = $eventData;
        $this->setTestName(self::TEST_NAME);
        $this->eventRequestsService = $eventRequestsService;
        $this->userRequestsService = $userRequestsService;
        $this->runTest();
    }

    public function testLoadMultipleEventObjects()
    {
        $eventIds = [
            $this->eventData->id,
            $this->eventData->id - 1,
            $this->eventData->id - 2,
        ];
        $this->eventRequestsService->setApiKey($this->userData->api_token);
        $this->setStepInput(["event_ids" => $eventIds]);
        $response = $this->eventRequestsService->loadMultipleEventObjects($eventIds);
        $output = json_decode($response->getContent(), true);
        $this->setStepOutput($output);
    }
}
