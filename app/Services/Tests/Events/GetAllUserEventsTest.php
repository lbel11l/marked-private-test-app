<?php

namespace App\Services\Tests;

use App\Services\Api\EventRequestsService;
use App\Services\Api\UserRequestsService;

class GetAllUserEventsTest extends WorkflowTestAbstract
{
    const TEST_NAME = "Get All User Events";
    protected $userRequestService;
    protected $eventRequestService;
    protected $userData;

    protected $steps = [
        "Get All User Events" => [],
    ];

    public function __construct(
        $userData,
        UserRequestsService $userRequestService,
        EventRequestsService $eventRequestService
    )
    {
        $this->userRequestService = $userRequestService;
        $this->eventRequestService = $eventRequestService;
        $this->userData = $userData;
        $this->setTestName(self::TEST_NAME);
        $this->runTest();
    }

    public function testGetAllUserEvents()
    {
        $this->eventRequestService->setApiKey($this->userData->api_token);
        $this->setStepInput(['userId' => $this->userData->id]);
        $response = $this->eventRequestService->getAllUserEvents();
        $this->setStepOutput(json_decode($response->getContent(), true));
    }
}
