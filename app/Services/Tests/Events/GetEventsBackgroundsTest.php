<?php

namespace App\Services\Tests;

use App\Services\Api\EventRequestsService;
use App\Services\Api\HostCodesRequestsService;

class GetEventsBackgroundsTest extends WorkflowTestAbstract
{
    const TEST_NAME = "Get Events Backgrounds";
    protected $eventRequestService;
    protected $userData;
    protected $eventData;


    protected $steps = [
        "Get Events Backgrounds" => [],
    ];

    public function __construct(
        $userData,
        $eventData,
        EventRequestsService $eventRequestService
    )
    {
        $this->eventRequestService = $eventRequestService;
        $this->userData = $userData;
        $this->eventData = $eventData;
        $this->setTestName(self::TEST_NAME);
        $this->runTest();
    }

    public function testGetEventsBackgrounds()
    {
        $eventIds = [
            $this->eventData->id,
            $this->eventData->id - 1,
            $this->eventData->id - 2
        ];
        $this->eventRequestService->setApiKey($this->userData->api_token);
        $this->setStepInput(["ids" => $eventIds]);
        $response = $this->eventRequestService->getEventsBackgrounds(["ids" => $eventIds]);
        $this->setStepOutput(json_decode($response->getContent(), true));
    }

}
