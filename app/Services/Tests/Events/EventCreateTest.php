<?php

namespace App\Services\Tests;

use App\Services\Api\EventRequestsService;
use App\Services\Api\HostCodesRequestsService;

class EventCreateTest extends WorkflowTestAbstract
{
    const TEST_NAME = "Create Event";
    protected $eventRequestService;
    protected $hostCodesRequestsService;
    protected $userData;

    protected $steps = [
        "Create Event"        => [],
    ];

    public function __construct(
        $userData,
        HostCodesRequestsService $hostCodesRequestsService,
        EventRequestsService $eventRequestService
    ) {
        $this->hostCodesRequestsService = $hostCodesRequestsService;
        $this->eventRequestService = $eventRequestService;
        $this->userData = $userData;
        $this->setTestName(self::TEST_NAME);
        $this->runTest();
    }

    public function testCreateEvent()
    {
        $inputParams = config('api_dummy_data.create_event');
        $inputParams['user_id'] = $this->userData->id;
        $codeData = $this->hostCodesRequestsService->getValidHostCode();
        $inputParams['code'][] = $codeData->code;
        $this->setStepInput($inputParams);
        $this->setVariables("codeData", $codeData);
        $this->eventRequestService->setApiKey($this->userData->api_token);
        $response = $this->eventRequestService->create($inputParams);
        $this->setStepOutput(json_decode($response->getContent(), true));
        $response->assertJson(
            [
                'id'               => true,
                'name'             => $inputParams['name'],
                'vendor_id'        => $inputParams['vendor_id'],
                'user_id'          => $inputParams['user_id'],
            ]
        );
    }

}
