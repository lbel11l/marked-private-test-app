<?php

namespace App\Services\Tests;

use App\Services\Api\EventRequestsService;
use App\Services\Api\UserRequestsService;

class UserEventsTest extends WorkflowTestAbstract
{
    const TEST_NAME = "User Events";
    protected $eventRequestsService;
    protected $userRequestsService;
    protected $userData;

    protected $steps = [
        "User Events" => [],
    ];

    public function __construct(
        $userData,
        UserRequestsService $userRequestsService,
        EventRequestsService $eventRequestsService
    )
    {
        $this->userData = $userData;
        $this->setTestName(self::TEST_NAME);
        $this->eventRequestsService = $eventRequestsService;
        $this->userRequestsService = $userRequestsService;
        $this->runTest();
    }

    public function testUserEvents()
    {
        $this->eventRequestsService->setApiKey($this->userData->api_token);
        $this->setStepInput(['userId' => $this->userData->id]);
        $response = $this->eventRequestsService->userEvents();
        $output = json_decode($response->getContent(), true);
        $this->setStepOutput($output);
        $response->assertJson(
            [
                'host' => [],
                'guest' => []
            ]
        );
    }
}
