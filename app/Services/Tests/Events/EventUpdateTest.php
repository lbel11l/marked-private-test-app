<?php

namespace App\Services\Tests;

use App\Services\Api\EventRequestsService;
use App\Services\Api\HostCodesRequestsService;

class EventUpdateTest extends WorkflowTestAbstract
{
    const TEST_NAME = "Update Event";
    protected $eventRequestService;
    protected $hostCodesRequestsService;
    protected $userData;
    protected $newData;
    protected $eventData;

    protected $steps = [
        "Update Event" => [],
    ];

    public function __construct(
        $userData,
        $newData,
        $eventData,
        HostCodesRequestsService $hostCodesRequestsService,
        EventRequestsService $eventRequestService
    )
    {
        $this->hostCodesRequestsService = $hostCodesRequestsService;
        $this->eventRequestService = $eventRequestService;
        $this->userData = $userData;
        $this->newData = $newData;
        $this->eventData = $eventData;
        $this->setTestName(self::TEST_NAME);
        $this->runTest();
    }

    public function testUpdateEvent()
    {
        $inputParams = config('api_dummy_data.create_event');
        $inputParams['user_id'] = $this->userData->id;
        $inputParams['code'][] = $this->hostCodesRequestsService->getValidHostCode();
        $inputParams = array_merge($inputParams, $this->newData);
        $this->setStepInput($inputParams);
        $this->eventRequestService->setApiKey($this->userData->api_token);
        $response = $this->eventRequestService->update($this->eventData->id, $inputParams);
        $this->setStepOutput(json_decode($response->getContent(), true));
        $response->assertJson(
            [
                'id' => true,
                'vendor_id' => $this->eventData->vendor_id,
                'name' => $inputParams['name'],
                'user_id' => $this->userData->id
            ]
        );
    }

}
