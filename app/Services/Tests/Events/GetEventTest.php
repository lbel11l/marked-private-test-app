<?php

namespace App\Services\Tests;

use App\Services\Api\EventRequestsService;
use App\Services\Api\HostCodesRequestsService;

class GetEventTest extends WorkflowTestAbstract
{
    const TEST_NAME = "Get Event";
    protected $eventRequestService;
    protected $hostCodesRequestsService;
    protected $userData;
    protected $eventData;


    protected $steps = [
        "Get Event"        => [],
    ];

    public function __construct(
        $userData,
        $eventData,
        HostCodesRequestsService $hostCodesRequestsService,
        EventRequestsService $eventRequestService
    ) {
        $this->hostCodesRequestsService = $hostCodesRequestsService;
        $this->eventRequestService = $eventRequestService;
        $this->userData = $userData;
        $this->eventData = $eventData;
        $this->setTestName(self::TEST_NAME);
        $this->runTest();
    }

    public function testGetEvent()
    {
        $this->eventRequestService->setApiKey($this->userData->api_token);
        $this->setStepInput(["event_id" => $this->eventData->id]);
        $response = $this->eventRequestService->getEvent($this->eventData->id);
        $this->setStepOutput(json_decode($response->getContent(), true));
        $response->assertJson(
            [
                'id'               => $this->eventData->id,
                'vendor_id'        => $this->eventData->vendor_id,
                'name'             => $this->eventData->name,
                'user_id'          => $this->userData->id
            ]
        );
    }

}
