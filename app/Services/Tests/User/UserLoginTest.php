<?php

namespace App\Services\Tests;

use App\Services\Api\HostCodesRequestsService;
use App\Services\Api\UserRequestsService;
use App\Services\ParametersBuilderService;

class UserLoginTest extends WorkflowTestAbstract
{
    const TEST_NAME = "User Login";
    protected $userRequestsService;
    protected $userData;

    protected $steps = [
        "User Login"        => [],
    ];

    /**
     * UserLoginTest constructor.
     * @param array $userData
     * @param UserRequestsService $userRequestsService
     */
    public function __construct(
        $userData,
        UserRequestsService $userRequestsService
    ) {
        $this->userData = $userData;
        $this->setTestName(self::TEST_NAME);
        $this->userRequestsService = $userRequestsService;
        $this->runTest();
    }

    public function testUserLogin()
    {
        $inputData = ['email' => $this->userData['email'], 'password' => $this->userData['password']];
        $this->setStepInput($inputData);
        $response = $this->userRequestsService->login($inputData);
        $output = json_decode($response->getContent(), true);
        $this->setStepOutput($output);
        $response->assertJson(
            [
                'id'               => true,
                'vendor_id'        => true,
                'email'            => $inputData['email'],
                'first_name'       => true,
                'last_name'        => true,
                'api_token'        => true,
                'profile_pic'      => true,
                'chat_credentials' => true,
            ]
        );
    }
}
