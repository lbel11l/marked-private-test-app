<?php

namespace App\Services\Tests;

use App\Services\Api\UserRequestsService;

class UserEditTest extends WorkflowTestAbstract
{
    const TEST_NAME = "User Edit";
    protected $userRequestsService;
    protected $userData;
    protected $newData;

    protected $steps = [
        "User Update" => [],
    ];

    public function __construct(
        $user,
        $newData,
        UserRequestsService $userRequestsService
    )
    {
        $this->userData = $user;
        $this->newData = $newData;
        $this->userRequestsService = $userRequestsService;
        $this->setTestName(self::TEST_NAME);
        $this->runTest();
    }

    public function testUserUpdate()
    {
        $inputData = array_merge(["user_id" => $this->userData->id], $this->newData);
        $this->userRequestsService->setApiKey($this->userData->api_token);
        $this->setStepInput($inputData);
        $response = $this->userRequestsService->update($this->userData->id, $inputData);
        $this->setStepOutput(json_decode($response->getContent(), true));
        $response->assertJson(
            [
                'vendor_id' => true,
                'first_name' => $inputData['first_name'],
                'last_name' => $inputData['last_name'],
                'api_token' => true,
                'profile_pic' => true,
                'chat_credentials' => true,
            ]
        );
    }
}
