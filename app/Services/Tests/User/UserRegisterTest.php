<?php

namespace App\Services\Tests;

use App\Services\Api\HostCodesRequestsService;
use App\Services\Api\UserRequestsService;
use App\Services\ParametersBuilderService;

class UserRegisterTest extends WorkflowTestAbstract
{
    const TEST_NAME = "User register";
    protected $userRequestsService;
    protected $parametersBuilderService;
    protected $hostCodesRequestsService;
    protected $userData;
    protected $loginData;

    protected $steps = [
        "User Register" => [],
    ];

    public function __construct(
        UserRequestsService $userRequestsService,
        HostCodesRequestsService $hostCodesRequestsService,
        ParametersBuilderService $parametersBuilderService
    )
    {
        $this->setTestName(self::TEST_NAME);
        $this->userRequestsService = $userRequestsService;
        $this->parametersBuilderService = $parametersBuilderService;
        $this->hostCodesRequestsService = $hostCodesRequestsService;
        $this->runTest();
    }

    public function testUserRegister()
    {
        $inputParams = config('api_dummy_data.user_register');
        $inputParams['email'] = str_random(5) . $inputParams['email'];
        $this->setStepInput($inputParams);
        $inputParams['profile_pic'] = $this->parametersBuilderService->pathToUploadedFile($inputParams['profile_pic']);
        $response = $this->userRequestsService->register($inputParams);
        $this->setStepOutput(json_decode($response->getContent(), true));
        $this->setVariables("loginData", $inputParams);
        $response->assertJson(
            [
                'vendor_id' => $inputParams['vendor_id'],
                'first_name' => $inputParams['first_name'],
                'last_name' => $inputParams['last_name'],
                'api_token' => true,
                'profile_pic' => true,
                'chat_credentials' => true,
            ]
        );

    }
}
