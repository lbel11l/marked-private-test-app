<?php

namespace App\Services\Tests;

use App\Services\Api\UserRequestsService;

class UserLogoutTest extends WorkflowTestAbstract
{
    const TEST_NAME = "User Logout";
    protected $userRequestsService;
    protected $userData;

    protected $steps = [
        "User Logout" => [],
    ];

    public function __construct(
        $userData,
        UserRequestsService $userRequestsService
    )
    {
        $this->userData = $userData;
        $this->setTestName(self::TEST_NAME);
        $this->userRequestsService = $userRequestsService;
        $this->runTest();
    }

    public function testUserLogout()
    {
        $this->userRequestsService->setApiKey($this->userData->api_token);
        $response = $this->userRequestsService->logout();
        $this->setStepOutput(json_decode($response->getContent(), true));
        $response->assertJson(
            [
                'message' => "User session deleted",
                'status_code' => 200
            ]
        );
    }
}
