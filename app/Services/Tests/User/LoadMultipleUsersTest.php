<?php

namespace App\Services\Tests;

use App\Services\Api\UserRequestsService;

class LoadMultipleUsersTest extends WorkflowTestAbstract
{
    const TEST_NAME = "Load Multiple Users";
    protected $userRequestsService;
    protected $userData;
    protected $newData;

    protected $steps = [
        "Load Multiple Users" => [],
    ];

    public function __construct(
        $userData,
        UserRequestsService $userRequestsService
    )
    {
        $this->userData = $userData;
        $this->setTestName(self::TEST_NAME);
        $this->userRequestsService = $userRequestsService;
        $this->runTest();
    }

    public function testLoadMultipleUsers()
    {
        $userIds = [
            $this->userData->id,
            $this->userData->id - 1,
            $this->userData->id - 2,
        ];
        $this->userRequestsService->setApiKey($this->userData->api_token);
        $this->setStepInput(["user_ids" => $userIds]);
        $response = $this->userRequestsService->loadMultipleUsers($userIds);
        $this->setStepOutput(json_decode($response->getContent(), true));
    }
}
