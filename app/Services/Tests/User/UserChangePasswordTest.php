<?php

namespace App\Services\Tests;

use App\Services\Api\UserRequestsService;

class UserChangePasswordTest extends WorkflowTestAbstract
{
    const TEST_NAME = "User Change Password";
    protected $userRequestsService;
    protected $userData;
    protected $newData;

    protected $steps = [
        "Change User Password"        => [],
    ];

    public function __construct(
        $userData,
        $newData,
        UserRequestsService $userRequestsService
    ) {
        $this->userData = $userData;
        $this->newData = $newData;
        $this->setTestName(self::TEST_NAME);
        $this->userRequestsService = $userRequestsService;
        $this->runTest();
    }

    public function testChangeUserPassword()
    {
        $inputData = [
            'email' => $this->userData->email,
            'old_password' => $this->userData->password,
            'new_password' => $this->newData['password']
        ];
        $this->userRequestsService->setApiKey($this->userData->api_token);
        $this->setStepInput($inputData);
        $response = $this->userRequestsService->changePassword($this->userData->id, $inputData);
        $this->setStepOutput(json_decode($response->getContent(), true));
        $response->assertJson(
            [
                'message' => "Entity updated"
            ]
        );
    }
}
