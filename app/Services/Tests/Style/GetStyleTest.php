<?php

namespace App\Services\Tests;

use App\Services\Api\UserRequestsService;

class GetStyleTest extends WorkflowTestAbstract
{
    const TEST_NAME = "Get Style";
    protected $userData;
    protected $styleData;
    protected $userRequestsService;

    protected $steps = [
        "Get Style" => [],
    ];

    public function __construct(
        $userData,
        $styleData,
        UserRequestsService $userRequestsService
    )
    {
        $this->userData = $userData;
        $this->styleData = $styleData;
        $this->setTestName(self::TEST_NAME);
        $this->userRequestsService = $userRequestsService;
        $this->runTest();
    }

    public function testGetStyle()
    {
        $styleId = $this->styleData->id;
        $this->setStepInput([$styleId]);
        $this->userRequestsService->setApiKey($this->userData->api_token);
        $response = $this->userRequestsService->getStyle($styleId);
        $this->setStepOutput(json_decode($response->getContent(), true));
        $response->assertJson(
            [
                'id' => true,
                'vendor_id' => true,
                'user_id' => true,
                'bg_color' => true,
                'font_family' => true,
                'font_color' => true,
            ]
        );
    }
}
