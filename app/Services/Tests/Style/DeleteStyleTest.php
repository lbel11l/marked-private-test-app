<?php

namespace App\Services\Tests;

use App\Services\Api\UserRequestsService;

class DeleteStyleTest extends WorkflowTestAbstract
{
    const TEST_NAME = "Delete Style";
    protected $userData;
    protected $styleData;
    protected $userRequestsService;

    protected $steps = [
        "Delete Style" => [],
    ];

    public function __construct(
        $userData,
        $styleData,
        UserRequestsService $userRequestsService
    )
    {
        $this->userData = $userData;
        $this->styleData = $styleData;
        $this->setTestName(self::TEST_NAME);
        $this->userRequestsService = $userRequestsService;
        $this->runTest();
    }

    public function testDeleteStyle()
    {
        $styleId = $this->styleData->id;
        $this->setStepInput([$styleId]);
        $this->userRequestsService->setApiKey($this->userData->api_token);
        $response = $this->userRequestsService->deleteStyle($styleId);
        $this->setStepOutput(json_decode($response->getContent(), true));
        $response->assertJson([
                'message' => "Entity deleted",
            ]
        );
    }
}
