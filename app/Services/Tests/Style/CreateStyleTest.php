<?php

namespace App\Services\Tests;

use App\Services\Api\StyleRequestService;
use App\Services\ParametersBuilderService;

class CreateStyleTest extends WorkflowTestAbstract
{
    const TEST_NAME = "Create Style";
    protected $userData;
    protected $styleRequestService;
    protected $parametersBuilderService;

    protected $steps = [
        "Create Style"        => [],
    ];

    public function __construct(
        $userData,
        StyleRequestService $styleRequestService,
        ParametersBuilderService $parametersBuilderService
    ) {
        $this->userData = $userData;
        $this->setTestName(self::TEST_NAME);
        $this->styleRequestService = $styleRequestService;
        $this->parametersBuilderService = $parametersBuilderService;
        $this->runTest();
    }

    public function testCreateStyle()
    {

        $inputData = array_merge(
            config("api_dummy_data.create_style"),
            [
                'vendor_id' => $this->userData->vendor_id,
                'user_id' => $this->userData->id
            ]
        );
        $this->setStepInput($inputData);
        $inputData['file'] = $this->parametersBuilderService->pathToUploadedFile($inputData['file']);
        $this->styleRequestService->setApiKey($this->userData->api_token);
        $response = $this->styleRequestService->createStyle($inputData);
        $this->setStepOutput(json_decode($response->getContent(), true));
        $response->assertJson(
            [
                'id'            => true,
                'vendor_id'     => $inputData['vendor_id'],
                'user_id'       => $inputData['user_id'],
                'bg_color'      => $inputData['bg_color'],
                'font_family'   => $inputData['font_family'],
                'font_color'    => $inputData['font_color'],
            ]
        );
    }
}
