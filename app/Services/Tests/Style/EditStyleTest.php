<?php

namespace App\Services\Tests;

use App\Services\Api\StyleRequestService;
use App\Services\ParametersBuilderService;

class EditStyleTest extends WorkflowTestAbstract
{
    const TEST_NAME = "Edit Style";
    protected $userData;
    protected $styleData;
    protected $eventData;
    protected $styleRequestService;
    protected $parametersBuilderService;

    protected $steps = [
        "Edit Style" => [],
    ];

    public function __construct(
        $userData,
        $styleData,
        $eventData,
        StyleRequestService $styleRequestService,
        ParametersBuilderService $parametersBuilderService
    )
    {
        $this->userData = $userData;
        $this->eventData = $eventData;
        $this->styleData = $styleData;
        $this->setTestName(self::TEST_NAME);
        $this->styleRequestService = $styleRequestService;
        $this->parametersBuilderService = $parametersBuilderService;
        $this->runTest();
    }

    public function testEditStyle()
    {

        $inputData = array_merge(
            config("api_dummy_data.create_style"),
            [
                'vendor_id' => $this->userData->vendor_id,
                'user_id' => $this->userData->id,
                'event_id' => $this->eventData->id
            ]
        );
        $this->setStepInput($inputData);
        $inputData['file'] = $this->parametersBuilderService->pathToUploadedFile($inputData['file']);
        $this->styleRequestService->setApiKey($this->userData->api_token);
        $response = $this->styleRequestService->editStyle($this->styleData->id, $inputData);
        $this->setStepOutput(json_decode($response->getContent(), true));
//        $response->assertJson(
//            [
//                'id' => true,
//                'vendor_id' => $inputData['vendor_id'],
//                'user_id' => $inputData['user_id'],
//                'bg_color' => $inputData['bg_color'],
//                'font_family' => $inputData['font_family'],
//                'font_color' => $inputData['font_color'],
//            ]
//        );
    }
}
