<?php

namespace App\Services\Tests;

use App\Services\Api\EventRequestsService;
use App\Services\ParametersBuilderService;

class RebuildChatDialogTest extends WorkflowTestAbstract
{
    const TEST_NAME = "Rebuild Chat Dialog";
    protected $userData;
    protected $eventData;
    protected $parametersBuilderService;
    protected $eventRequestService;

    protected $steps = [
        "Rebuild Chat Dialog" => [],
    ];

    public function __construct(
        $userData,
        $eventData,
        EventRequestsService $eventRequestService,
        ParametersBuilderService $parametersBuilderService
    )
    {
        $this->userData = $userData;
        $this->eventData = $eventData;
        $this->setTestName(self::TEST_NAME);
        $this->parametersBuilderService = $parametersBuilderService;
        $this->eventRequestService = $eventRequestService;
        $this->runTest();
    }

    public function testRebuildChatDialog()
    {
        $this->setStepInput([$this->eventData->id]);
        $this->eventRequestService->setApiKey($this->userData->api_token);
        $response = $this->eventRequestService->rebuildChatDialog($this->eventData->id);
        $output = json_decode($response->getContent(), true);
        $this->setStepOutput($output);
        $response->assertJson(
            [
                'id'               => true,
                'vendor_id'        => true,
                'user_id'          => true,
                'dialog_id'        => true,
            ]
        );
    }
}
