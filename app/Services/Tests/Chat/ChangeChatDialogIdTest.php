<?php

namespace App\Services\Tests;

use App\Services\Api\EventRequestsService;
use App\Services\ParametersBuilderService;

class ChangeChatDialogIdTest extends WorkflowTestAbstract
{
    const TEST_NAME = "Change Chat Dialog Id";
    protected $userData;
    protected $eventData;
    protected $parametersBuilderService;
    protected $eventRequestService;

    protected $steps = [
        "Change Chat Dialog Id" => [],
    ];

    public function __construct(
        $userData,
        $eventData,
        EventRequestsService $eventRequestService,
        ParametersBuilderService $parametersBuilderService
    )
    {
        $this->userData = $userData;
        $this->eventData = $eventData;
        $this->setTestName(self::TEST_NAME);
        $this->parametersBuilderService = $parametersBuilderService;
        $this->eventRequestService = $eventRequestService;
        $this->runTest();
    }

    public function testChangeChatDialogId()
    {
        $inputData = config("api_dummy_data.chat_id");
        $this->setStepInput(array_merge(["event_id" => $this->eventData->id,], $inputData));
        $this->eventRequestService->setApiKey($this->userData->api_token);
        $response = $this->eventRequestService->changeChatDialogId($this->eventData->id, $inputData);
        $this->setStepOutput(json_decode($response->getContent(), true));
        $response->assertJson(
            [
                'id'               => true,
                'vendor_id'        => true,
                'user_id'          => true,
                'dialog_id'        => $inputData['dialog_id'],
            ]
        );
    }
}
