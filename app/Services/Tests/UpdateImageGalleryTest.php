<?php

namespace App\Services\Tests;

use App\Services\Api\EventRequestsService;
use App\Services\Api\ImageRequestsService;
use App\Services\ParametersBuilderService;

class UpdateImageGalleryTest extends WorkflowTestAbstract
{
    const TEST_NAME = "Update Image Gallery";
    protected $userData;
    protected $galleryData;
    protected $eventData;
    protected $imageRequestService;
    protected $parametersBuilderService;

    protected $steps = [
        "Update Image Gallery"        => [],
    ];

    public function __construct(
        $userData,
        $galleryData,
        $eventData,
        ImageRequestsService $imageRequestService,
        ParametersBuilderService $parametersBuilderService
    ) {
        $this->userData = $userData;
        $this->galleryData = $galleryData;
        $this->eventData = $eventData;
        $this->setTestName(self::TEST_NAME);
        $this->imageRequestService = $imageRequestService;
        $this->parametersBuilderService = $parametersBuilderService;
        $this->runTest();
    }

    public function testUpdateImageGallery()
    {

        $inputData = array_merge(
            config("api_dummy_data.upload_file"),
            [
                'vendor_id' => $this->userData->vendor_id
            ]
        );
        $this->setStepInput($inputData);
        $inputData['file'] = $this->parametersBuilderService->pathToUploadedFile($inputData['file']);
        $this->imageRequestService->setApiKey($this->userData->api_token);
        $response = $this->imageRequestService->updateImageGallery($this->eventData->id, $this->galleryData->id, $inputData);
        $this->setStepOutput(json_decode($response->getContent(), true));
        $response->assertJson(
            [
                'id'               => true,
                'file_id'        => true,
                'event_id'        => true,
            ]
        );
    }
}
