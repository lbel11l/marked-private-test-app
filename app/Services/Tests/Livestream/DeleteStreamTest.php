<?php

namespace App\Services\Tests;

use App\Services\Api\StreamRequestsService;
use App\Services\Api\UserRequestsService;

class DeleteStreamTest extends WorkflowTestAbstract
{
    const TEST_NAME = "Delete Stream";
    protected $userRequestsService;
    protected $streamRequestsService;
    protected $userData;
    protected $eventData;

    protected $steps = [
        "Delete Stream" => [],
    ];

    public function __construct(
        $userData,
        $eventData,
        UserRequestsService $userRequestsService,
        StreamRequestsService $streamRequestsService
    )
    {
        $this->userData = $userData;
        $this->eventData = $eventData;
        $this->setTestName(self::TEST_NAME);
        $this->userRequestsService = $userRequestsService;
        $this->streamRequestsService = $streamRequestsService;
        $this->runTest();
    }

    public function testDeleteStream()
    {
        $inputData = [
            'vendor_id' => $this->eventData->vendor_id,
            'user_id' => $this->eventData->user_id,
            'event_id' => $this->eventData->id
        ];
        $this->setStepInput($inputData);
        $this->streamRequestsService->setApiKey($this->userData->api_token);
        $response = $this->streamRequestsService->deleteStream($inputData);
        $this->setStepOutput(json_decode($response->getContent(), true));
        $response->assertJson(
            [
                'message' => "Entity deleted"
            ]
        );
    }
}
