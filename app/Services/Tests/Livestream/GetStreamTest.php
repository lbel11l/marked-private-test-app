<?php

namespace App\Services\Tests;

use App\Services\Api\StreamRequestsService;
use App\Services\Api\UserRequestsService;

class GetStreamTest extends WorkflowTestAbstract
{
    const TEST_NAME = "Get Stream";
    protected $userRequestsService;
    protected $streamRequestsService;
    protected $userData;
    protected $eventData;

    protected $steps = [
        "Get Stream" => [],
    ];

    public function __construct(
        $userData,
        $eventData,
        UserRequestsService $userRequestsService,
        StreamRequestsService $streamRequestsService
    )
    {
        $this->userData = $userData;
        $this->eventData = $eventData;
        $this->setTestName(self::TEST_NAME);
        $this->userRequestsService = $userRequestsService;
        $this->streamRequestsService = $streamRequestsService;
        $this->runTest();
    }

    public function testGetStream()
    {
        $inputData = [
            'vendor_id' => $this->eventData->vendor_id,
            'user_id' => $this->eventData->user_id,
            'event_id' => $this->eventData->id
        ];
        $this->setStepInput($inputData);
        $this->streamRequestsService->setApiKey($this->userData->api_token);
        $response = $this->streamRequestsService->getStream($this->eventData->id, $inputData);
        $output = json_decode($response->getContent(), true);
        $this->setStepOutput($output);
        $response->assertJson(
            [
                'vendor_id' => $inputData['vendor_id'],
                'user_id' => $inputData['user_id'],
                'event_id' => $inputData['event_id'],
                'created_at' => true,
                'publish_token' => true,
                'stream_name' => true,
            ]
        );
    }
}
