<?php

namespace App\Services\Clients;

use GuzzleHttp\Client;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Foundation\Testing\TestResponse;
use Illuminate\Http\Response;
use Psr\Http\Message\ResponseInterface;

class ImageClient extends AbstractClient
{
    function __construct()
    {
        $this->setBaseURI(env('IMAGE_API_URL'));
    }


    public function createStyle($params)
    {
        $response = $this->requestHandler(
            FormRequest::METHOD_POST,
            'style?api_token=' . $this->apiKey,
            [
                'multipart' => $params,
            ]
        );
        return $this->responseHandler($response);
    }

    public function createEventStyle($eventId, $params)
    {
        $response = $this->requestHandler(
            FormRequest::METHOD_POST,
            'event/'.$eventId.'/styles?api_token=' . $this->apiKey,
            [
                'multipart' => $params,
            ]
        );
        return $this->responseHandler($response);
    }

    public function editStyle($styleId, $params)
    {
        $response = $this->requestHandler(
            FormRequest::METHOD_POST,
            'style/' . $styleId . '?api_token=' . $this->apiKey,
            [
                'multipart' => $params,
            ]
        );
        return $this->responseHandler($response);
    }

    public function createImageGallery($eventId, $params)
    {
        $response = $this->requestHandler(
            FormRequest::METHOD_POST,
            'event/' . $eventId . '/gallery?api_token=' . $this->apiKey,
            [
                'multipart' => $params,
            ]
        );
        return $this->responseHandler($response);
    }

    public function updateImageGallery($eventId, $galleryId, $params)
    {
        $response = $this->requestHandler(
            FormRequest::METHOD_POST,
            'gallery/' . $galleryId . '?api_token=' . $this->apiKey,
            [
                'multipart' => $params,
            ]
        );
        return $this->responseHandler($response);
    }

}
