<?php

namespace App\Services\Clients;

use GuzzleHttp\Client;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Foundation\Testing\TestResponse;
use Illuminate\Http\Response;
use Psr\Http\Message\ResponseInterface;

class HostCodeClient extends AbstractClient
{
    function __construct()
    {
        $this->setBaseURI(env('CODES_URL'));
    }

    public function getValidHostCode($route)
    {
        $response = $this->requestHandler(FormRequest::METHOD_GET, $route);
        return json_decode($response->getBody()->getContents())[0];
    }

    public function getCode($route, $hostCode)
    {
        $response = $this->requestHandler(FormRequest::METHOD_GET, $route);
        return json_decode($response->getBody()->getContents());
    }
}
