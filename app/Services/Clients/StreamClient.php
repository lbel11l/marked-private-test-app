<?php

namespace App\Services\Clients;

use GuzzleHttp\Client;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Foundation\Testing\TestResponse;
use Illuminate\Http\Response;
use Psr\Http\Message\ResponseInterface;

class StreamClient extends AbstractClient
{
    function __construct()
    {
        $this->setBaseURI(env('VIDEO_API_URL'));
    }

    public function createStream($params)
    {
        $response = $this->requestHandler(
            FormRequest::METHOD_POST,
            'live?api_token=' . $this->apiKey,
            [
                'form_params' => $params,
            ]
        );

        return $this->responseHandler($response);
    }

    public function getStream($eventId, $params)
    {
        $response = $this->requestHandler(
            FormRequest::METHOD_GET,
            'live/' . $eventId .'?api_token=' . $this->apiKey,
            [
                'form_params' => $params,
            ]
        );
        return $this->responseHandler($response);
    }

    public function deleteStream($params)
    {
        $response = $this->requestHandler(
            FormRequest::METHOD_DELETE,
            'live?api_token=' . $this->apiKey,
            [
                'form_params' => $params,
            ]
        );
        return $this->responseHandler($response);
    }

    public function createVideoGallery($eventId, $params)
    {
        $response = $this->requestHandler(
            FormRequest::METHOD_POST,
            'event/' . $eventId . '/gallery?api_token=' . $this->apiKey,
            [
                'multipart' => $params,
            ]
        );
        return $this->responseHandler($response);
    }

    public function galleryVideosCreateMultipleCopy($eventId, $params)
    {
        $response = $this->requestHandler(
            FormRequest::METHOD_POST,
            'event/' . $eventId . '/galleries?api_token=' . $this->apiKey,
            [
                'multipart' => $params,
            ]
        );
        return $this->responseHandler($response);
    }
}
