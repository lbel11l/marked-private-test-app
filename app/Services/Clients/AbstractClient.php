<?php

namespace App\Services\Clients;

use GuzzleHttp\Client;
use Illuminate\Foundation\Testing\TestResponse;
use Illuminate\Http\Response;
use Psr\Http\Message\ResponseInterface;

class AbstractClient
{
    protected $client;
    protected $refresh;
    protected $apiKey;
    protected $baseURI;



    protected function initClient()
    {
        $options = [
            'base_uri' => $this->baseURI,
            'headers'  => [
//                'Authorization'  => 'Bearer ' . ,
                'Accept'         => 'application/json',
            ],
        ];

        $this->client = new Client($options);
    }
    /**
     * Get HTTP client.
     *
     * @return Client
     */
    public function getClient()
    {
        if (!$this->client ||$this->refresh) {
            $this->initClient();
        }
        $this->refresh = false;
        return $this->client;
    }

    public function requestHandler($method, $uri, $options = [])
    {
        try {
            $client = $this->getClient();
            $response = $client->request($method, $uri, $options);
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage(),$e->getCode());
        }

        return $response;
    }


    public function responseHandler(ResponseInterface $response)
    {
        return TestResponse::fromBaseResponse(Response::create($response->getBody()->getContents()));
    }

    /**
     * @param mixed $refresh
     */
    public function setRefresh($refresh)
    {
        $this->refresh = $refresh;
    }

    /**
     * @param mixed $apiKey
     */
    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;
        $this->refresh = true;
    }

    /**
     * @return mixed
     */
    public function getBaseURI()
    {
        return $this->baseURI;
    }

    /**
     * @param mixed $baseURI
     */
    public function setBaseURI($baseURI)
    {
        $this->baseURI = $baseURI;
    }
}
