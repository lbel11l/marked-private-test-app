<?php

namespace App\Services\Clients;

use Illuminate\Foundation\Http\FormRequest;

class MarkedPrivateClient extends AbstractClient
{
    function __construct()
    {
        $this->setBaseURI(env('BACK_API_URL'));
    }

    public function register(array $params){

        $response = $this->requestHandler(
            FormRequest::METHOD_POST,
            'register',
            [
                'multipart' => $params,
            ]
        );
        return $this->responseHandler($response);
    }

    public function login(array $params){
        $response = $this->requestHandler(
            FormRequest::METHOD_POST,
            'login',
            [
                'form_params' => $params,
            ]
        );
        return $this->responseHandler($response);
    }

    public function logout(){

        $response = $this->requestHandler(
            FormRequest::METHOD_POST,
            'logout?api_token=' . $this->apiKey
        );

        return $this->responseHandler($response);
    }

    public function updateUser($userId, array $params){

        $response = $this->requestHandler(
            FormRequest::METHOD_POST,
            'user/' . $userId . '?api_token=' . $this->apiKey,
            [
                'multipart' => $params,
            ]
        );
        return $this->responseHandler($response);
    }

    public function updateEvent($eventId, $params)
    {
        $response = $this->requestHandler(
            FormRequest::METHOD_PATCH,
            'event/' . $eventId . '?api_token=' . $this->apiKey,
            [
                'form_params' => $params,
            ]
        );
        return $this->responseHandler($response);
    }

    public function loadMultipleUsers($userIds)
    {
        $response = $this->requestHandler(
            FormRequest::METHOD_POST,
            'users?api_token=' . $this->apiKey,
            [
                'form_params' => [
                    'user_ids' => $userIds,
                ],
            ]
        );
        return $this->responseHandler($response);
    }

    public function loadMultipleEventObjects($eventIds)
    {
        $response = $this->requestHandler(
            FormRequest::METHOD_POST,
            'events?api_token=' . $this->apiKey,
            [
                'form_params' => [
                    'event_ids' => $eventIds,
                ],
            ]
        );
        return $this->responseHandler($response);
    }

    public function changePassword($userId, array $params){

        $response = $this->requestHandler(
            FormRequest::METHOD_PUT,
            'user/' . $userId .'/reset' . '?api_token=' . $this->apiKey,
            [
                'form_params' => $params,
            ]
        );
        return $this->responseHandler($response);
    }

    public function userEvents()
    {
        $response = $this->requestHandler(
            FormRequest::METHOD_GET,
            'user/events?api_token='.$this->apiKey
        );
        return $this->responseHandler($response);
    }
    public function checkCodes($codes){

        $response = $this->requestHandler(
            FormRequest::METHOD_POST,
            'code/check',
            [
                'form_params' => [
                    "codes" => $codes
                ],
            ]
        );
        return $this->responseHandler($response);
    }

    public function addGuestCode($eventId, $guestCode)
    {
        $response = $this->requestHandler(
            FormRequest::METHOD_POST,
            'event/' . $eventId . '/guest?api_token=' . $this->apiKey,
            [
                'form_params' => [
                    'code' => $guestCode
                ]
            ]
        );
        return $this->responseHandler($response);
    }

    public function createEvent($params)
    {
        $response = $this->requestHandler(
            FormRequest::METHOD_POST,
            'event?api_token='.$this->apiKey,
            [
                'form_params' => $params,
            ]
        );
        return $this->responseHandler($response);
    }

    public function getEvent($eventId)
    {
        $response = $this->requestHandler(
            FormRequest::METHOD_GET,
            'event/' . $eventId . '?api_token='.$this->apiKey
        );
        return $this->responseHandler($response);
    }

    public function getAllUserEvents()
    {
        $response = $this->requestHandler(
            FormRequest::METHOD_GET,
            'user/allevents?api_token=' . $this->apiKey
        );
        return $this->responseHandler($response);
    }

    /*public function getEventRSVPS($eventId)
    {
        $response = $this->requestHandler(
            FormRequest::METHOD_GET,
            'event/' . $eventId . '/' . $rsvpId .'api_token=' . $this->apiKey
        );
        return $this->responseHandler($response);
    }*/

    public function createTicket($params)
    {
        $response = $this->requestHandler(
            FormRequest::METHOD_POST,
            'ticket',
            [
                'form_params' => $params,
            ]
        );
        return $this->responseHandler($response);
    }

    public function getStyle($styleId)
    {
        $response = $this->requestHandler(
            FormRequest::METHOD_GET,
            'style/' . $styleId . '?api_token=' . $this->apiKey
        );
        return $this->responseHandler($response);
    }

    public function deleteStyle($styleId)
    {
        $response = $this->requestHandler(
            FormRequest::METHOD_DELETE,
            'style/' . $styleId . '?api_token=' . $this->apiKey
        );
        return $this->responseHandler($response);
    }

    public function createDescriptionAsset($eventId, $params)
    {
        $response = $this->requestHandler(
            FormRequest::METHOD_POST,
            'event/' . $eventId . '/descr?api_token=' . $this->apiKey,
            [
                'multipart' => $params,
            ]
        );
        return $this->responseHandler($response);
    }

    public function getDescriptionAsset($eventId)
    {
        $response = $this->requestHandler(
            FormRequest::METHOD_GET,
            'event/' . $eventId . '/descr?api_token=' . $this->apiKey
        );
        return $this->responseHandler($response);
    }

    public function getAllEventGalleries($eventId)
    {
        $response = $this->requestHandler(
            FormRequest::METHOD_GET,
            'event/' . $eventId . '/gallery?api_token=' . $this->apiKey
        );
        return $this->responseHandler($response);
    }

    public function updateGallery($eventId, $galleryId, $params)
    {
        $response = $this->requestHandler(
            FormRequest::METHOD_POST,
            'event/' . $eventId . '/gallery/' . $galleryId . '?api_token=' . $this->apiKey,
            [
                'multipart' => $params,
            ]
        );
        return $this->responseHandler($response);
    }

    public function deleteGallery($eventId, $galleryId)
    {
        $response = $this->requestHandler(
            FormRequest::METHOD_DELETE,
            'event/' . $eventId . '/gallery/' . $galleryId . '?api_token=' . $this->apiKey
        );
        return $this->responseHandler($response);
    }

    public function deleteMultipleGalleries($eventId, $galleryIds)
    {
        $response = $this->requestHandler(
            FormRequest::METHOD_DELETE,
            'event/' . $eventId . '/galleries?api_token=' . $this->apiKey,
            [
                'form_params' => [
                    'ids' => $galleryIds,
                ],
            ]
        );
        return $this->responseHandler($response);
    }

    public function getAllEventRSVP($eventId)
    {
        $response = $this->requestHandler(
            FormRequest::METHOD_GET,
            'event/' . $eventId . '/rsvp?api_token=' . $this->apiKey
        );
        return $this->responseHandler($response);
    }

    public function getEventRSVP($eventId, $rsvpId)
    {
        $response = $this->requestHandler(
            FormRequest::METHOD_GET,
            'event/' . $eventId . '/rsvp/' . $rsvpId . '?api_token=' . $this->apiKey
        );
        return $this->responseHandler($response);
    }

    public function updateEventRSVP($eventId, $rsvpId, $params)
    {
        $response = $this->requestHandler(
            FormRequest::METHOD_PATCH,
            'event/' . $eventId . '/rsvp/' . $rsvpId . '?api_token=' . $this->apiKey,
            [
                'form_params' => $params,
            ]
        );
        return $this->responseHandler($response);
    }

    public function getEventsBackgrounds($params)
    {
        $response = $this->requestHandler(
            FormRequest::METHOD_POST,
            'event-gallery-background?api_token=' . $this->apiKey,
            [
                'form_params' => $params,
            ]
        );
        return $this->responseHandler($response);
    }

    public function rebuildChatDialog($eventId)
    {
        $response = $this->requestHandler(
            FormRequest::METHOD_POST,
            'event/' . $eventId . '/chat/rebuild?api_token=' . $this->apiKey
        );
        return $this->responseHandler($response);
    }

    public function changeChatDialogId($eventId, $params)
    {
        $response = $this->requestHandler(
            FormRequest::METHOD_POST,
            'event/' . $eventId . '/chat?api_token=' . $this->apiKey,
            [
                'form_params' => $params,
            ]
        );
        return $this->responseHandler($response);
    }

}
