<?php

namespace App\Services\Workflows;

use App\Http\Models\Event;
use App\Http\Models\Stream;
use App\Http\Models\Ticket;
use App\Services\Tests\TestingServiceAbstract;
use App\User;

class TicketFlow extends TestingServiceAbstract
{
    const TYPE_HOST = 'host';

    protected $host;
    protected $event;

    function __construct()
    {
        $this->host = User::where('type', '=', self::TYPE_HOST)->first();
    }

    public function execute()
    {
        Ticket::truncate();

        $this->createTicket();



        return $this->testResults;
    }

    public function createTicket()
    {
        $this->runTest(
            'createTicket',
            [
                'userData' => $this->host,
            ]
        );
        $ticket = new Ticket($this->testResults['createTicket']['Output']);
        $ticket->save();
    }
}
