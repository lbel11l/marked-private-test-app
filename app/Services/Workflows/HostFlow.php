<?php

namespace App\Services\Workflows;

use App\Http\Models\Event;
use App\Http\Models\User;
use App\Services\Tests\TestingServiceAbstract;

class HostFlow extends TestingServiceAbstract
{
    const PASSWORD = 'password';

    protected $registeredUser;
    protected $createdEvent;
    protected $createdStyle;
    protected $createdGallery;
    protected $createdRSVP;
    protected $codeData;


    public function execute()
    {
        $this->registerUser();
        $this->loginUser();
//        $this->editProfile();
//        $this->changePassword();
//        $this->loadMultipleUsers();
        $this->createEvent();
//        $this->userEvents();
//        $this->getAllUserEvents();
//        $this->getEvent();
//        $this->updateEvent();
//        $this->loadMultipleEventObjects();
//        $this->createDescriptionAsset();
//        $this->getDescriptionAsset();
        $this->addGuestCode();
//        $this->createStream();
//        $this->getStream();
        $this->deleteStream();
        $this->createTicket();
        $this->createStyle();
        $this->createEventStyle();
        $this->getStyle();
        $this->editStyle();
        $this->deleteStyle();
        $this->createImageGallery();
        $this->createVideoGallery();
        $this->getAllEventGalleries();
        $this->updateGallery();
        $this->deleteGallery();
        $this->deleteMultipleGalleries();
//        $this->galleryVideosCreateMultipleCopy();
        $this->getEventsBackgrounds();
        $this->rebuildChatDialog();
        $this->changeChatDialogId();
//        $this->getAllEventRSVP();
//        $this->getEventRSVP();
//        $this->updateEventRSVP();

//      ######   LAST ######
        $this->logoutUser();

        return $this->testResults;
    }

    public function registerUser()
    {
        $this->runTest('userRegister');
        $this->registeredUser = new User($this->testResults['userRegister']['Output']);
        $this->registeredUser->password = self::PASSWORD;
    }

    public function loginUser()
    {
        $this->runTest(
            'userLogin',
            [
                "userData" =>
                    [
                        'email' => $this->registeredUser->email,
                        'password' => self::PASSWORD
                    ]
            ]
        );
    }

    public function logoutUser()
    {
        $this->runTest(
            'userLogout',
            [
                "userData" => $this->registeredUser
            ]
        );
    }

    public function editProfile()
    {
        $this->runTest(
            'userEdit',
            [
                'user' => $this->registeredUser,
                'newData' => ['first_name' => "da", 'last_name' => 'daaa']
            ]
        );
    }

    public function createEvent()
    {
        $this->runTest(
            'createEvent',
            [
                'userData' => $this->registeredUser
            ]
        );

        $this->createdEvent = new Event($this->testResults['createEvent']['Output']);
        $this->codeData = $this->testResults['createEvent']['Variables']['codeData'];
    }

    public function getEvent()
    {
        $this->runTest(
            'getEvent',
            [
                'userData' => $this->registeredUser,
                'eventData' => $this->createdEvent
            ]
        );
    }

    public function updateEvent()
    {
        $this->runTest(
            'updateEvent',
            [
                'userData' => $this->registeredUser,
                'eventData' => $this->createdEvent,
                'newData' => [
                    'name' => "daaaa",
                    'start_date' => '2017-05-02 11:00:00',
                    'end_date' => '2017-06-02 19:00:00'
                ],
            ]
        );
    }

    public function changePassword()
    {
        $this->runTest(
            'changePassword',
            [
                'userData' => $this->registeredUser,
                'newData' => ['password' => 'newPassword']
            ]
        );
    }

    public function loadMultipleUsers()
    {
        $this->runTest(
            'loadMultipleUsers',
            [
                'userData' => $this->registeredUser
            ]
        );
    }

    public function loadMultipleEventObjects()
    {
        $this->runTest(
            'loadMultipleEventObjects',
            [
                'userData' => $this->registeredUser,
                'eventData' => $this->createdEvent
            ]
        );
    }

    public function userEvents()
    {
        $this->runTest(
            'userEvents',
            [
                'userData' => $this->registeredUser
            ]
        );
    }

    public function getAllUserEvents()
    {
        $this->runTest(
            'getAllUserEvents',
            [
                'userData' => $this->registeredUser
            ]
        );
    }

    public function addGuestCode()
    {
        $this->runTest(
            'addGuestCode',
            [
                'userData' => $this->registeredUser,
                'eventData' => $this->createdEvent,
                'codeData' =>$this->codeData
            ]
        );
    }

    public function createStream()
    {
        $this->runTest(
            'createStream',
            [
                'userData' => $this->registeredUser,
                'eventData' => $this->createdEvent
            ]
        );
    }

    public function getStream()
    {
        $this->runTest(
            'getStream',
            [
                'userData' => $this->registeredUser,
                'eventData' => $this->createdEvent
            ]
        );
    }

    public function deleteStream()
    {
        $this->runTest(
            'deleteStream',
            [
                'userData' => $this->registeredUser,
                'eventData' => $this->createdEvent
            ]
        );
    }

    public function createTicket()
    {
        $this->runTest(
            'createTicket',
            [
                'userData' => $this->registeredUser,
            ]
        );
    }

    public function createStyle()
    {
        $this->runTest(
            'createStyle',
            [
                'userData' => $this->registeredUser,
            ]
        );
        $this->createdStyle = new Event($this->testResults['createStyle']['Output']);
    }

    public function createEventStyle()
    {
        $this->runTest(
            'createEventStyle',
            [
                'userData' => $this->registeredUser,
                'eventData' => $this->createdEvent,
            ]
        );
    }

    public function editStyle()
    {
        $this->runTest(
            'editStyle',
            [
                'userData' => $this->registeredUser,
                'styleData' => $this->createdStyle,
                'eventData' => $this->createdEvent,
            ]
        );
    }

    public function createVideoGallery()
    {
        $this->runTest(
            'createVideoGallery',
            [
                'userData' => $this->registeredUser,
                'eventData' => $this->createdEvent,
            ]
        );
    }

    public function getStyle()
    {
        $this->runTest(
            'getStyle',
            [
                'userData' => $this->registeredUser,
                'styleData' => $this->createdStyle,
            ]
        );
    }

    public function deleteStyle()
    {
        $this->runTest(
            'deleteStyle',
            [
                'userData' => $this->registeredUser,
                'styleData' => $this->createdStyle,
            ]
        );
    }

    public function createDescriptionAsset()
    {
        $this->runTest(
            'createDescriptionAsset',
            [
                'userData' => $this->registeredUser,
                'eventData' => $this->createdEvent,
            ]
        );
    }

    public function getDescriptionAsset()
    {
        $this->runTest(
            'getDescriptionAsset',
            [
                'userData' => $this->registeredUser,
                'eventData' => $this->createdEvent,
            ]
        );
    }

    private function createImageGallery()
    {
        $this->runTest(
            'createImageGallery',
            [
                'userData' => $this->registeredUser,
                'eventData' => $this->createdEvent,
            ]
        );
        $this->createdGallery = new Event($this->testResults['createImageGallery']['Output']);
    }

    private function getAllEventGalleries()
    {
        $this->runTest(
            'getAllEventGalleries',
            [
                'userData' => $this->registeredUser,
                'eventData' => $this->createdEvent,
            ]
        );
    }

    private function updateGallery()
    {
        $this->runTest(
            'updateGallery',
            [
                'userData' => $this->registeredUser,
                'galleryData' => $this->createdGallery,
            ]
        );
    }

    private function deleteGallery()
    {
        $this->runTest(
            'deleteGallery',
            [
                'userData' => $this->registeredUser,
                'eventData' => $this->createdEvent,
                'galleryData' => $this->createdGallery,
            ]
        );
    }

    public function galleryVideosCreateMultipleCopy()
    {
        $this->runTest(
            'galleryVideosCreateMultipleCopy',
            [
                'userData' => $this->registeredUser,
                'eventData' => $this->createdEvent,
            ]
        );
    }

    public function deleteMultipleGalleries()
    {
        $this->runTest(
            'deleteMultipleGalleries',
            [
                'userData' => $this->registeredUser,
                'eventData' => $this->createdEvent,
                'galleryData' => $this->createdGallery,
            ]
        );
    }

    private function getAllEventRSVP()
    {
        $this->runTest(
            'getAllEventRSVP',
            [
                'userData' => $this->registeredUser,
                'eventData' => $this->createdEvent,
            ]
        );
    }

    private function getEventRSVP()
    {
        $this->runTest(
            'getEventRSVP',
            [
                'userData' => $this->registeredUser,
                'eventData' => $this->createdEvent,
            ]
        );

        $this->createdRSVP = new Event($this->testResults['getEventRSVP']['Output']);
    }

    private function updateEventRSVP()
    {
        $this->runTest(
            'updateEventRSVP',
            [
                'userData' => $this->registeredUser,
                'eventData' => $this->createdEvent,
                'rsvpData' => $this->createdRSVP,
            ]
        );
    }

    private function getEventsBackgrounds()
    {
        $this->runTest(
            'getEventsBackgrounds',
            [
                'userData' => $this->registeredUser,
                'eventData' => $this->createdEvent
            ]
        );
    }

    public function rebuildChatDialog()
    {
        $this->runTest(
            'rebuildChatDialog',
            [
                'userData' => $this->registeredUser,
                'eventData' => $this->createdEvent
            ]
        );
    }

    public function changeChatDialogId()
    {
        $this->runTest(
            'changeChatDialogId',
            [
                'userData' => $this->registeredUser,
                'eventData' => $this->createdEvent
            ]
        );
    }
}
