<?php

namespace App\Services\Workflows;

use App\Http\Models\Event;
use App\Http\Models\Stream;
use App\Services\Tests\TestingServiceAbstract;
use App\User;

class StreamFlow extends TestingServiceAbstract
{
    const TYPE_HOST = 'host';

    protected $host;
    protected $event;
    protected $guest;
    protected $stream;

    function __construct()
    {
        $this->host = User::where('type', '=', self::TYPE_HOST)->first();
        $this->event = Event::where('user_id', '=', $this->host->id)->first();
    }

    public function execute()
    {
        Stream::truncate();
        //CRUD on streams
        $this->createStream();
        $this->getStream();
        $this->deleteStream();

        return $this->testResults;
    }

    public function createStream()
    {
        $this->runTest(
            'createStream',
            [
                'userData' => $this->host,
                'eventData' => $this->event
            ]
        );
        $stream = new Stream($this->testResults['createStream']['Output']);
        $this->stream = $stream;
        $stream->save();
    }

    public function getStream()
    {
        $this->runTest(
            'getStream',
            [
                'userData' => $this->host,
                'eventData' => $this->event
            ]
        );
    }

    public function deleteStream()
    {
        $this->runTest(
            'deleteStream',
            [
                'userData' => $this->host,
                'eventData' => $this->event
            ]
        );

        Stream::where('stream_name', '=', $this->stream->stream_name)->delete();
    }


}
