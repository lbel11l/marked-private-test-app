<?php

namespace App\Services\Workflows;

use App\Http\Models\Event;
use App\Http\Models\Gallery;
use App\Http\Models\Stream;
use App\Services\Tests\TestingServiceAbstract;
use App\User;

class GalleryFlow extends TestingServiceAbstract
{
    const TYPE_HOST = 'host';

    protected $host;
    protected $event;
    protected $gallery;

    function __construct()
    {
        $this->host = User::where('type', '=', self::TYPE_HOST)->first();
        $this->event = Event::where('user_id', '=', $this->host->id)->first();
    }

    public function execute()
    {
        Gallery::truncate();
        //CRUD on galleries
        $this->createImageGallery();
        $this->createVideoGallery();
        $this->getAllEventGalleries();
//        $this->updateGallery();
        $this->updateImageGallery();
        $this->galleryVideosCreateMultipleCopy();
        $this->deleteGallery();
        $this->deleteMultipleGalleries();

        return $this->testResults;
    }


    private function createImageGallery()
    {
        $this->runTest(
            'createImageGallery',
            [
                'userData' => $this->host,
                'eventData' => $this->event,
            ]
        );
        $this->gallery = new Gallery($this->testResults['createImageGallery']['Output']);
        $this->gallery->save();
    }

    public function createVideoGallery()
    {
        $this->runTest(
            'createVideoGallery',
            [
                'userData' => $this->host,
                'eventData' => $this->event,
            ]
        );
        $this->gallery = new Gallery($this->testResults['createVideoGallery']['Output']);
        $this->gallery->save();
    }

    private function getAllEventGalleries()
    {
        $this->runTest(
            'getAllEventGalleries',
            [
                'userData' => $this->host,
                'eventData' => $this->event,
            ]
        );
    }

    private function updateGallery()
    {
        $this->runTest(
            'updateGallery',
            [
                'userData' => $this->host,
                'galleryData' => $this->gallery,
                'eventData' => $this->event,
            ]
        );
//        $this->gallery->password = $this->testResults['updateGallery']['Output']['new_password'];
//        $this->gallery->save();
    }

    private function updateImageGallery()
    {
        $this->runTest(
            'updateImageGallery',
            [
                'userData' => $this->host,
                'galleryData' => $this->gallery,
                'eventData' => $this->event,
            ]
        );
//        $this->gallery->password = $this->testResults['updateGallery']['Output']['new_password'];
//        $this->gallery->save();
    }

    private function deleteGallery()
    {
        $this->runTest(
            'deleteGallery',
            [
                'userData' => $this->host,
                'eventData' => $this->event,
                'galleryData' => $this->gallery,
            ]
        );
        Gallery::where('id', '=', $this->gallery->id)->where('event_id', '=', $this->event->id)->delete();
    }

    public function galleryVideosCreateMultipleCopy()
    {
        $this->runTest(
            'galleryVideosCreateMultipleCopy',
            [
                'userData' => $this->host,
                'eventData' => $this->event,
            ]
        );
    }

    public function deleteMultipleGalleries()
    {
        $this->runTest(
            'deleteMultipleGalleries',
            [
                'userData' => $this->host,
                'eventData' => $this->event,
                'galleryData' => $this->gallery,
            ]
        );
        Gallery::where('id', '=', $this->gallery->id)->where('event_id', '=', $this->event->id)->delete();
        Gallery::where('id', '=', $this->gallery->id - 1)->where('event_id', '=', $this->event->id)->delete();
    }























    public function createStream()
    {
        $this->runTest(
            'createStream',
            [
                'userData' => $this->host,
                'eventData' => $this->event
            ]
        );
        $stream = new Stream($this->testResults['createStream']['Output']);
        $this->stream = $stream;
        $stream->save();
    }

    public function getStream()
    {
        $this->runTest(
            'getStream',
            [
                'userData' => $this->host,
                'eventData' => $this->event
            ]
        );
    }

    public function deleteStream()
    {
        $this->runTest(
            'deleteStream',
            [
                'userData' => $this->host,
                'eventData' => $this->event
            ]
        );

        Stream::where('stream_name', '=', $this->stream->stream_name)->delete();
    }


}
