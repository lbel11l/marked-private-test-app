<?php

namespace App\Services\Workflows;

use App\Http\Models\Event;
use App\Services\Tests\TestingServiceAbstract;
use App\User;

class EventFlow extends TestingServiceAbstract
{
    const TYPE_HOST = 'host';
    const TYPE_GUEST = 'guest';

    protected $host;
    protected $event;
    protected $guest;

    function __construct()
    {
        $this->host = User::where('type', '=', self::TYPE_HOST)->first();
        $this->guest =  User::where('type', '=', self::TYPE_GUEST)->first();
    }

    public function execute()
    {
        Event::truncate();
        //CRUD on event
        $this->createEvent();
        $this->getEvent();
        $this->updateEvent();
        //event actions
        $this->userEvents();
        $this->getAllUserEvents();
        $this->addGuestCode();
        $this->getEventsBackgrounds();
        $this->rebuildChatDialog();
        $this->changeChatDialogId();


        return $this->testResults;
    }

    public function createEvent()
    {
        $this->runTest(
            'createEvent',
            [
                'userData' => $this->host
            ]
        );

        $event = new Event($this->testResults['createEvent']['Output']);
        $codeData = $this->testResults['createEvent']['Variables']['codeData'];
        $event->hostcodes = $codeData;
        $event->save();
        $this->event = $event;
    }

    public function getEvent()
    {
        $this->runTest(
            'getEvent',
            [
                'userData'  => $this->host,
                'eventData' => $this->event
            ]
        );
    }

    public function updateEvent()
    {
        /** @var Event $event */
        $this->runTest(
            'updateEvent',
            [
                'userData' => $this->host,
                'eventData' => $this->event,
                'newData' => [
                    'name' => "daaaa",
                    'start_date' => '2017-05-02 11:00:00',
                    'end_date' => '2017-06-02 19:00:00'
                ],
            ]
        );
        $this->event->name = $this->testResults['updateEvent']['Output']['name'];
        $this->event->start_date = $this->testResults['updateEvent']['Output']['start_date'];
        $this->event->end_date = $this->testResults['updateEvent']['Output']['end_date'];
        $this->event->save();
    }

    public function userEvents()
    {
        $this->runTest(
            'userEvents',
            [
                'userData' => $this->host
            ]
        );
    }

    public function getAllUserEvents()
    {
        $this->runTest(
            'getAllUserEvents',
            [
                'userData' => $this->host
            ]
        );
    }

    public function addGuestCode()
    {
        $this->runTest(
            'addGuestCode',
            [
                'userData' => $this->guest,
                'eventData' => $this->event
            ]
        );
        $this->event->rsvps = $this->testResults['addGuestCode']['Output']['rsvps'];
        $this->event->save();
    }

    private function getEventsBackgrounds()
    {
        $this->runTest(
            'getEventsBackgrounds',
            [
                'userData'  => $this->host,
                'eventData' => $this->event
            ]
        );
    }

    public function rebuildChatDialog()
    {
        $this->runTest(
            'rebuildChatDialog',
            [
                'userData'  => $this->host,
                'eventData' => $this->event
            ]
        );
    }

    public function changeChatDialogId()
    {
        $this->runTest(
            'changeChatDialogId',
            [
                'userData'  => $this->host,
                'eventData' => $this->event
            ]
        );
    }
}
