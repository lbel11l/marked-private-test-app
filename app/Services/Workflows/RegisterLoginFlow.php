<?php

namespace App\Services\Workflows;

use App\Http\Models\User;
use App\Services\Tests\TestingServiceAbstract;

class RegisterLoginFlow extends TestingServiceAbstract
{
    const PASSWORD = 'password';
    const TYPE_HOST = 'host';
    const TYPE_GUEST = 'guest';

    protected $hostUser;
    protected $guestUser;

    function __construct()
    {
        $this->hostUser = User::where('type', '=', self::TYPE_HOST)->first();
        $this->guestUser = User::where('type', '=', self::TYPE_GUEST)->first();
    }

    public function execute()
    {
        User::truncate();
        // register users
        $this->registerHostUser();
        $this->registerGuestUser();
        // login users
        $this->loginHostUser();
        $this->loginGuestUser();
        //edit user
        $this->changePassword();
        $this->editProfile();
        //user host code
        $this->checkHostCode();

        return $this->testResults;
    }

    public function registerHostUser()
    {
        $this->runTest('userRegister');
        $user = new User($this->testResults['userRegister']['Output']);
        $user->password = self::PASSWORD;
        $user->type = self::TYPE_HOST;
        $user->save();
    }

    public function registerGuestUser()
    {
        $this->runTest('userRegister');
        $user = new User($this->testResults['userRegister']['Output']);
        $user->password = self::PASSWORD;
        $user->type = self::TYPE_GUEST;
        $user->save();
    }

    public function loginHostUser()
    {
        /** @var User $user */
        $this->runTest(
            'userLogin',
            [
                "userData" =>
                    [
                        'email' => $this->hostUser->email,
                        'password' => self::PASSWORD
                    ]
            ]
        );

        $this->hostUser->api_token = $this->testResults['userLogin']['Output']['api_token'];
        $this->hostUser->save();
    }

    public function loginGuestUser()
    {
        /** @var User $user */
        $this->runTest(
            'userLogin',
            [
                "userData" =>
                    [
                        'email' => $this->guestUser->email,
                        'password' => self::PASSWORD
                    ]
            ]
        );
        $this->guestUser->api_token = $this->testResults['userLogin']['Output']['api_token'];
        $this->guestUser->save();
    }

    public function changePassword()
    {
        /** @var User $user */
        $this->runTest(
            'changePassword',
            [
                'userData' => $this->hostUser,
                'newData' => ['password' => 'newPassword']
            ]
        );
        $this->hostUser->password = $this->testResults['changePassword']['Input']['new_password'];
        $this->hostUser->save();
    }

    public function editProfile()
    {
        $this->runTest(
            'userEdit',
            [
                'user' => $this->hostUser,
                'newData' => ['first_name' => "da", 'last_name' => 'daaa']
            ]
        );
        $this->hostUser->first_name = $this->testResults['userEdit']['Output']['first_name'];
        $this->hostUser->last_name = $this->testResults['userEdit']['Output']['last_name'];
        $this->hostUser->save();
    }

    public function checkHostCode()
    {
        $this->runTest("hostCode");
    }
}
