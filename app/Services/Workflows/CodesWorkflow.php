<?php

namespace App\Services\Workflows;

use App\Services\Tests\TestingServiceAbstract;

class CodesWorkflow extends TestingServiceAbstract
{
    function __construct()
    {
        return $this->execute();
    }

    public function execute()
    {
        return $this->runTest("hostCode");
    }
}
