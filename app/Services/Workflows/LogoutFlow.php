<?php

namespace App\Services\Workflows;

use App\Http\Models\User;
use App\Services\Tests\TestingServiceAbstract;

class LogoutFlow extends TestingServiceAbstract
{
    const PASSWORD = 'password';
    const TYPE_HOST = 'host';
    const TYPE_GUEST = 'guest';

    protected $hostUser;
    protected $guestUser;

    function __construct()
    {
        $this->hostUser = User::where('type', '=', self::TYPE_HOST)->first();
        $this->guestUser = User::where('type', '=', self::TYPE_GUEST)->first();
    }

    public function execute()
    {
        $this->logoutHostUser();
        $this->logoutGuestUser();

        return $this->testResults;
    }

    public function logoutHostUser()
    {
        $this->runTest(
            'userLogout',
            [
                "userData" => $this->hostUser
            ]
        );
    }

    public function logoutGuestUser()
    {
        $this->runTest(
            'userLogout',
            [
                "userData" => $this->guestUser
            ]
        );
    }
}
