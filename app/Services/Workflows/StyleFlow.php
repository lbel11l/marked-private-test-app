<?php

namespace App\Services\Workflows;

use App\Http\Models\Event;
use App\Http\Models\Stream;
use App\Http\Models\Style;
use App\Services\Tests\TestingServiceAbstract;
use App\User;

class StyleFlow extends TestingServiceAbstract
{
    const TYPE_HOST = 'host';

    protected $host;
    protected $event;
    protected $guest;
    protected $style;

    function __construct()
    {
        $this->host = User::where('type', '=', self::TYPE_HOST)->first();
        $this->event = Event::where('user_id', '=', $this->host->id)->first();
    }

    public function execute()
    {
        Style::truncate();
        //CRUD on styles
        $this->createStyle();
        $this->createEventStyle();
        $this->getStyle();
        $this->editStyle();

        return $this->testResults;
    }

    public function createStyle()
    {
        $this->runTest(
            'createStyle',
            [
                'userData' => $this->host,
            ]
        );
        $style = new Style($this->testResults['createStyle']['Output']);
        $this->style = $style;
        $style->save();
    }

    public function createEventStyle()
    {
        $this->runTest(
            'createEventStyle',
            [
                'userData' => $this->host,
                'eventData' => $this->event,
            ]
        );
    }

    public function editStyle()
    {
        $this->runTest(
            'editStyle',
            [
                'userData' => $this->host,
                'styleData' => $this->style,
                'eventData' => $this->event,
            ]
        );
    }

    public function getStyle()
    {
        $this->runTest(
            'getStyle',
            [
                'userData' => $this->host,
                'styleData' => $this->style,
            ]
        );
    }
}