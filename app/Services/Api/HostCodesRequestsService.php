<?php
/**
 * Created by PhpStorm.
 * User: sergiu
 * Date: 4/25/17
 * Time: 3:59 PM
 */

namespace App\Services\Api;


use App\Services\Clients\HostCodeClient;
use App\Services\Clients\MarkedPrivateClient;

class HostCodesRequestsService
{
    protected $apiClient;
    protected $hostCodeClient;
    protected $paramBuilder;

    function __construct(MarkedPrivateClient $apiClient, HostCodeClient $hostCodeClient)
    {
        $this->apiClient = $apiClient;
        $this->hostCodeClient = $hostCodeClient;
    }

    public function getValidHostCode(){
        return $this->hostCodeClient->getValidHostCode("mock-codes-list");
    }

    public function getGuestCode($hostCode)
    {
        return $this->hostCodeClient->getCode("mock-codes-list", $hostCode);
    }

    public function checkCodes($codes){
        $this->apiClient->setRefresh(true);
        return $this->apiClient->checkCodes($codes);
    }

}
