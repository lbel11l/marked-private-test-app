<?php
/**
 * Created by PhpStorm.
 * User: sergiu
 * Date: 4/25/17
 * Time: 3:59 PM
 */

namespace App\Services\Api;


use App\Services\Clients\ImageClient;
use App\Services\ParametersBuilderService;

class ImageRequestsService
{
    protected $apiClient;
    protected $paramBuilder;

    function __construct(ImageClient $apiClient, ParametersBuilderService $paramBuilder)
    {
        $this->apiClient = $apiClient;
        $this->paramBuilder = $paramBuilder;
    }

    public function createImageGallery($eventId, $params)
    {
        return $this->apiClient->createImageGallery($eventId, $this->paramBuilder->paramsToMultipart($params));
    }

    public function updateImageGallery($eventId, $galleryId, $params)
    {
        return $this->apiClient->updateImageGallery($eventId, $galleryId, $this->paramBuilder->paramsToMultipart($params));
    }


    /**
     * @param mixed $apiKey
     */
    public function setApiKey($apiKey)
    {
        $this->apiClient->setApiKey($apiKey);
    }


}
