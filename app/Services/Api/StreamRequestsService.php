<?php
/**
 * Created by PhpStorm.
 * User: sergiu
 * Date: 4/25/17
 * Time: 3:59 PM
 */

namespace App\Services\Api;


use App\Services\Clients\MarkedPrivateClient;
use App\Services\Clients\StreamClient;
use App\Services\ParametersBuilderService;

class StreamRequestsService
{
    protected $apiClient;
    protected $paramBuilder;

    function __construct(StreamClient $apiClient, ParametersBuilderService $paramBuilder)
    {
        $this->apiClient = $apiClient;
        $this->paramBuilder = $paramBuilder;
    }

    public function createStream($params)
    {

        return $this->apiClient->createStream($params);
    }

    public function getStream($eventId, $params)
    {
        return $this->apiClient->getStream($eventId, $params);
    }

    public function deleteStream($params)
    {
        return $this->apiClient->deleteStream($params);
    }

    public function createVideoGallery($eventId, $params)
    {
        return $this->apiClient->createVideoGallery($eventId, $this->paramBuilder->paramsToMultipart($params));
    }

    public function galleryVideosCreateMultipleCopy($eventId, $params)
    {
        return $this->apiClient->galleryVideosCreateMultipleCopy($eventId, $this->paramBuilder->paramsToMultipart($params));
    }

    /**
     * @param mixed $apiKey
     */
    public function setApiKey($apiKey)
    {
        $this->apiClient->setApiKey($apiKey);
    }


}
