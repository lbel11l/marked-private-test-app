<?php
/**
 * Created by PhpStorm.
 * User: sergiu
 * Date: 4/25/17
 * Time: 3:59 PM
 */

namespace App\Services\Api;


use App\Services\Clients\MarkedPrivateClient;
use App\Services\ParametersBuilderService;

class UserRequestsService
{
    protected $apiClient;
    protected $paramBuilder;

    function __construct(MarkedPrivateClient $apiClient, ParametersBuilderService $paramBuilder)
    {
        $this->apiClient = $apiClient;
        $this->paramBuilder = $paramBuilder;
    }

    public function register($params)
    {
        return $this->apiClient->register($this->paramBuilder->paramsToMultipart($params));
    }

    public function login($params)
    {
        return $this->apiClient->login($params);
    }

    public function logout()
    {
        return $this->apiClient->logout();
    }

    public function update($userId, $params)
    {
        return $this->apiClient->updateUser($userId, $this->paramBuilder->paramsToMultipart($params));
    }

    public function changePassword($userId, $params)
    {
        return $this->apiClient->changePassword($userId, $params);
    }

    public function loadMultipleUsers($userIds)
    {
        return $this->apiClient->loadMultipleUsers($userIds);
    }

    public function getStyle($styleId)
    {
        return $this->apiClient->getStyle($styleId);
    }

    public function deleteStyle($styleId)
    {
        return $this->apiClient->deleteStyle($styleId);
    }

    public function createDescriptionAsset($eventId, $params)
    {
        return $this->apiClient->createDescriptionAsset($eventId, $this->paramBuilder->paramsToMultipart($params));
    }

    public function getDescriptionAsset($eventId)
    {
        return $this->apiClient->getDescriptionAsset($eventId);
    }

    /**
     * @param mixed $apiKey
     */
    public function setApiKey($apiKey)
    {
        $this->apiClient->setApiKey($apiKey);
    }

}
