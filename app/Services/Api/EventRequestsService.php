<?php
/**
 * Created by PhpStorm.
 * User: sergiu
 * Date: 4/25/17
 * Time: 3:59 PM
 */

namespace App\Services\Api;


use App\Services\Clients\MarkedPrivateClient;
use App\Services\ParametersBuilderService;

class EventRequestsService
{
    protected $apiClient;
    protected $paramBuilder;

    function __construct(MarkedPrivateClient $apiClient, ParametersBuilderService $paramBuilder)
    {
        $this->apiClient = $apiClient;
        $this->paramBuilder = $paramBuilder;
    }

    public function create($params)
    {
        return $this->apiClient->createEvent($params);
    }

    public function getEvent($eventId)
    {
        return $this->apiClient->getEvent($eventId);
    }


    public function update($eventId, $params)
    {
        return $this->apiClient->updateEvent($eventId, $params);
    }

    public function loadMultipleEventObjects($eventIds)
    {
        return $this->apiClient->loadMultipleEventObjects($eventIds);
    }

    public function addGuestCode($eventId, $guestCode)
    {
        return $this->apiClient->addGuestCode($eventId, $guestCode);
    }

    public function userEvents()
    {
        return $this->apiClient->userEvents();
    }

    public function getAllUserEvents()
    {
        return $this->apiClient->getAllUserEvents();
    }

    public function getAllEventGalleries($eventId)
    {
        return $this->apiClient->getAllEventGalleries($eventId);
    }

    public function deleteGallery($eventId, $galleryId)
    {
        return $this->apiClient->deleteGallery($eventId, $galleryId);
    }

    public function updateGallery($eventId, $galleryId, $params)
    {
        return $this->apiClient->updateGallery($eventId, $galleryId, $this->paramBuilder->paramsToMultipart($params));
    }


    public function deleteMultipleGalleries($eventId, $galleryIds)
    {
        return $this->apiClient->deleteMultipleGalleries($eventId, $galleryIds);
    }

    public function getAllEventRSVP($eventId)
    {
        return $this->apiClient->getAllEventRSVP($eventId);
    }

    public function getEventRSVP($eventId, $rsvpId)
    {
        return $this->apiClient->getEventRSVP($eventId, $rsvpId);
    }

    public function updateEventRSVP($eventId, $rsvpId, $params)
    {
        return $this->apiClient->updateEventRSVP($eventId, $rsvpId, $params);
    }

    public function getEventsBackgrounds($params)
    {
        return $this->apiClient->getEventsBackgrounds($params);
    }

    public function rebuildChatDialog($eventId)
    {
        return $this->apiClient->rebuildChatDialog($eventId);
    }

    public function changeChatDialogId($eventId, $params)
    {
        return $this->apiClient->changeChatDialogId($eventId, $params);
    }
    /**
     * @param mixed $apiKey
     */
    public function setApiKey($apiKey)
    {
        $this->apiClient->setApiKey($apiKey);
    }


}
