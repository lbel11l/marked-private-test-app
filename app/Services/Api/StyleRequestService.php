<?php
/**
 * Created by PhpStorm.
 * User: sergiu
 * Date: 4/25/17
 * Time: 3:59 PM
 */

namespace App\Services\Api;


use App\Services\Clients\ImageClient;
use App\Services\Clients\MarkedPrivateClient;
use App\Services\ParametersBuilderService;

class StyleRequestService
{
    protected $apiClient;
    protected $paramBuilder;

    function __construct(ImageClient $apiClient, ParametersBuilderService $paramBuilder)
    {
        $this->apiClient = $apiClient;
        $this->paramBuilder = $paramBuilder;
    }

    public function createStyle($params)
    {
        return $this->apiClient->createStyle($this->paramBuilder->paramsToMultipart($params));
    }

    public function createEventStyle($eventId, $params)
    {
        return $this->apiClient->createEventStyle($eventId, $this->paramBuilder->paramsToMultipart($params));
    }

    public function editStyle($eventId, $params)
    {
        return $this->apiClient->editStyle($eventId, $this->paramBuilder->paramsToMultipart($params));
    }

    /**
     * @param mixed $apiKey
     */
    public function setApiKey($apiKey)
    {
        $this->apiClient->setApiKey($apiKey);
    }

}
