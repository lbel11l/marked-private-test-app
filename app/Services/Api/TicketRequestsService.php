<?php
/**
 * Created by PhpStorm.
 * User: sergiu
 * Date: 4/25/17
 * Time: 3:59 PM
 */

namespace App\Services\Api;


use App\Services\Clients\MarkedPrivateClient;
use App\Services\ParametersBuilderService;

class TicketRequestsService
{
    protected $apiClient;
    protected $paramBuilder;

    function __construct(MarkedPrivateClient $apiClient, ParametersBuilderService $paramBuilder)
    {
        $this->apiClient = $apiClient;
        $this->paramBuilder = $paramBuilder;
    }

    public function createTicket($params)
    {

        return $this->apiClient->createTicket($params);
    }

    /**
     * @param mixed $apiKey
     */
    public function setApiKey($apiKey)
    {
        $this->apiClient->setApiKey($apiKey);
    }


}
